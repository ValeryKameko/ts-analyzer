import { Dictionary } from 'typescript-collections';
import { ASTNode, ASTProgram, ASTStatement, ASTBreakStatement, ASTContinueStatement, ASTReturnStatement, ASTExpression, ASTExpressionStatement, ASTEmptyStatement, ASTBlockStatement, ASTIfStatement, ASTWhileStatement, ASTForStatement, ASTDoWhileStatement, ASTExpressionType, ASTFunctionCallParameters, ASTFunctionDeclarationStatement, ASTIdentifier, ASTLiteral, ASTLiteralType, ASTNumberLiteral, ASTStringLiteral, ASTPrimitiveLiteral, ASTVariableDeclarationStatement, ASTSwitchStatement, ASTCaseClause, ASTDefaultClause } from '../AST';

export class HalsteadMetrics {
    public operandsFrequencyTable: Dictionary<string, number> = new Dictionary();
    public operatorsFrequencyTable: Dictionary<string, number> = new Dictionary();
    constructor() {
    }

    addOperand(operand: string) {
        this.operandsFrequencyTable.setValue(
            operand,
            this.getOperandCount(operand) + 1
        );
    }

    addOperator(operator: string) {
        this.operatorsFrequencyTable.setValue(
            operator,
            this.getOperatorCount(operator) + 1
        );
    }

    getOperatorCount(operator: string): number {
        let count = this.operatorsFrequencyTable.getValue(operator);
        if (!count)
            count = 0;
        return count;
    }

    getOperandCount(operand: string): number {
        let count = this.operandsFrequencyTable.getValue(operand);
        if (!count)
            count = 0;
        return count;
    }

    toString(): string {
        let operandsKeyValues: [string, number][] = [];
        this.operandsFrequencyTable.forEach((key, value) => {
            operandsKeyValues.push([key, value]);
        });
        let operatorsKeyValues: [string, number][] = [];
        this.operatorsFrequencyTable.forEach((key, value) => {
            operatorsKeyValues.push([key, value]);
        });
        let resultString = "";
        resultString += this.toTable(["OPERATORS", "COUNT"], ["n1", "N1"], operatorsKeyValues);
        resultString += "\n\n\n\n";
        resultString += this.toTable(["OPERANDS", "COUNT"], ["n2", "N2"], operandsKeyValues);

        let n1 = operatorsKeyValues.length;
        let N1 = 0;
        for (const [key, value] of operatorsKeyValues)
            N1 += value;
        let n2 = operandsKeyValues.length;
        let N2 = 0;
        for (const [key, value] of operandsKeyValues)
            N2 += value;

        resultString += `Словарь программы \tn = ${n1 + n2}\n`;
        resultString += `Длина программы   \tN = ${N1 + N2}\n`;
        resultString += `Объем программы   \tV = ${(N1 + N2) * Math.log2(n1 + n2)}\n`;
        return resultString;
    }

    private toTable(titles: string[], count_titles: string[], keyValues: [string, number][]): string {
        const WIDTH = 50;
        let table = "";
        table += this.tableSeparatorRow(2, WIDTH, "-") + "\n";
        table += this.tableRow(titles, WIDTH) + "\n";
        table += this.tableSeparatorRow(2, WIDTH, "-") + "\n";
        for (const keyValue of keyValues) {
            table += this.tableRow([keyValue[0], Number(keyValue[1]).toString()], WIDTH) + "\n";
            table += this.tableSeparatorRow(2, WIDTH, "-") + "\n";
        }
        let n = keyValues.length
        let N = 0;
        for (const [key, value] of keyValues)
            N += value;
        table += this.tableRow([`${count_titles[0]} = ${n}`, `${count_titles[1]} = ${N}`], WIDTH) + '\n';
        return table;
    }
    private tableRow(values: string[], width: number): string {
        let row = "";
        row += "|";
        for (const value of values) {
            let slices = this.slicify(value, width);
            row += ' ' + value + ' '.repeat(width - value.length - 1);
            row += "|";
        }
        return row;
    }

    private slicify(str: string, width: number): string[] {
        let slices: string[] = [];
        let currString = str;
        while (currString != "") {
            slices.push(currString.substr(0, width));
            currString = currString.substr(width);
        }
        return slices;
    }

    private tableSeparatorRow(count: number, width: number, fillChar: string = ' '): string {
        let row = "+";
        for (let i = 0; i < count; i++) {
            row += fillChar.repeat(width);
            row += "+"
        }
        return row;
    }
}

export class HalsteadMetricsAnalyzer {
    private metrics: HalsteadMetrics = new HalsteadMetrics();
    constructor() {
    }

    calcMetrics(node: ASTNode): HalsteadMetrics {
        this.metrics = new HalsteadMetrics();
        this.calcASTNode(node);
        return this.metrics;
    }

    private calcASTNode(node: ASTNode) {
        if (node instanceof ASTProgram)
            return this.calcASTProgram(node);
    }

    private calcASTProgram(node: ASTProgram) {
        for (const statement of node.statements)
            this.calcASTStatement(<ASTStatement>statement);
    }

    private calcASTStatement(node: ASTStatement): void {
        if (node instanceof ASTVariableDeclarationStatement) {
            for (const variableDeclaration of node.variableDeclarations) {
                if (variableDeclaration.initializer)
                    this.calcASTExpression(variableDeclaration.initializer)
            }
            return;
        }
        if (node instanceof ASTBreakStatement)
            return this.metrics.addOperator("break");
        if (node instanceof ASTContinueStatement)
            return this.metrics.addOperator("continue");
        if (node instanceof ASTReturnStatement) {
            this.metrics.addOperator("return");
            return this.calcASTExpression(node.expression);
        }
        if (node instanceof ASTExpressionStatement)
            return this.calcASTExpression(node.expression);
        if (node instanceof ASTEmptyStatement)
            return this.metrics.addOperator(";");
        if (node instanceof ASTBlockStatement) {
            for (const statement of node.statements)
                this.calcASTStatement(statement);
            return this.metrics.addOperator("{...}");
        }
        if (node instanceof ASTIfStatement) {
            this.metrics.addOperator("if (...) ... else ...");

            this.calcASTExpression(node.condition);
            for (const statement of node.ifStatements)
                this.calcASTStatement(statement);
            for (const statement of node.elseStatements)
                this.calcASTStatement(statement);
            return;
        }
        if (node instanceof ASTWhileStatement) {
            this.metrics.addOperator("while (...) ...");
            this.calcASTExpression(node.condition);
            return this.calcASTStatement(node.body);
        }
        if (node instanceof ASTForStatement) {
            this.metrics.addOperator("for(...; ...; ...) ...");
            if (node.initializer)
                this.calcASTStatement(node.initializer);
            if (node.condition)
                this.calcASTExpression(node.condition);
            if (node.afterExpression)
                this.calcASTExpression(node.afterExpression);
            return this.calcASTStatement(node.body);
        }
        if (node instanceof ASTDoWhileStatement) {
            this.metrics.addOperator("do ... while (...)");
            this.calcASTExpression(node.condition);
            return this.calcASTStatement(node.body);
        }
        if (node instanceof ASTFunctionDeclarationStatement) {
            return this.calcASTStatement(node.body);
        }
        if (node instanceof ASTSwitchStatement) {
            this.calcASTExpression(node.condition);
            for (const clause of node.clauses) {
                if (clause instanceof ASTCaseClause) {
                    this.calcASTExpression(clause.expression);
                    for (const statement of clause.statements)
                        this.calcASTStatement(statement);
                }
                if (clause instanceof ASTDefaultClause)
                    for (const statement of clause.statements)
                        this.calcASTStatement(statement);
            }
            return;
        }
    }

    private calcASTExpression(node: ASTExpression) {
        if (![
            ASTExpressionType.FunctionCall,
            ASTExpressionType.Literal,
            ASTExpressionType.Identifier,
        ].includes(node.expressionType)) {
            for (const operand of node.operands)
                this.calcASTExpression(<ASTExpression>operand);
        }
        switch (node.expressionType) {
            case ASTExpressionType.Assignment:
                this.metrics.addOperator("=");
                break;
            case ASTExpressionType.BitwiseAnd:
                this.metrics.addOperator("&");
                break;
            case ASTExpressionType.BitwiseAndAssignment:
                this.metrics.addOperator("&=");
                break;
            case ASTExpressionType.BitwiseNot:
                this.metrics.addOperator("~");
                break;
            case ASTExpressionType.BitwiseOr:
                this.metrics.addOperator("|");
                break;
            case ASTExpressionType.BitwiseOrAssignment:
                this.metrics.addOperator("|=");
                break;
            case ASTExpressionType.BitwiseXor:
                this.metrics.addOperator("^");
                break;
            case ASTExpressionType.BitwiseXorAssignment:
                this.metrics.addOperator("^=");
                break;
            case ASTExpressionType.BracketsEnclosed:
                this.metrics.addOperator("(...)");
                break;
            case ASTExpressionType.DeleteOperator:
                this.metrics.addOperator("delete ...");
                break;
            case ASTExpressionType.Division:
                this.metrics.addOperator("/");
                break;
            case ASTExpressionType.DivisionAssignment:
                this.metrics.addOperator("/=");
                break;
            case ASTExpressionType.FunctionCall:
                let functionCallName = this.toStringFunctionCallName(<ASTExpression>node.operands[0]);
                this.metrics.addOperator(`${functionCallName}(...)`);
                let functionCallParameters = <ASTFunctionCallParameters>node.operands[1];
                for (const parameter of functionCallParameters.parameters) {
                    this.calcASTExpression(<ASTExpression>parameter);
                }
                break;
            case ASTExpressionType.Greater:
                this.metrics.addOperator(">");
                break;
            case ASTExpressionType.GreaterOrEqual:
                this.metrics.addOperator(">=");
                break;
            case ASTExpressionType.Identifier:
                let identifier = (<ASTIdentifier>node.operands[0]).identifier;
                this.metrics.addOperand(identifier);
                break;
            case ASTExpressionType.InOperator:
                this.metrics.addOperator("in");
                break;
            case ASTExpressionType.InstanceOfOperator:
                this.metrics.addOperator("instanceof");
                break;
            case ASTExpressionType.Less:
                this.metrics.addOperator("<");
                break;
            case ASTExpressionType.LessOrEqual:
                this.metrics.addOperator("<=");
                break;
            case ASTExpressionType.Literal:
                this.metrics.addOperand(
                    this.calcASTLiteral(<ASTLiteral>node.operands[0])
                );
                break;
            case ASTExpressionType.LogicalAnd:
                this.metrics.addOperator("&&");
                break;
            case ASTExpressionType.LogicalNot:
                this.metrics.addOperator("!");
                break;
            case ASTExpressionType.LogicalOr:
                this.metrics.addOperator("|");
                break;
            case ASTExpressionType.LogicalShiftLeft:
                this.metrics.addOperator("<<");
                break;
            case ASTExpressionType.LogicalShiftLeftAssignment:
                this.metrics.addOperator("<<=");
                break;
            case ASTExpressionType.LogicalShiftRight:
                this.metrics.addOperator(">>");
                break;
            case ASTExpressionType.LogicalShiftRightAssignment:
                this.metrics.addOperator(">>=");
                break;
            case ASTExpressionType.Modulo:
                this.metrics.addOperator("%");
                break;
            case ASTExpressionType.ModuloAssignment:
                this.metrics.addOperator("%=");
                break;
            case ASTExpressionType.Multiply:
                this.metrics.addOperator("*");
                break;
            case ASTExpressionType.MultiplyAssignment:
                this.metrics.addOperator("*=");
                break;
            case ASTExpressionType.NotEqual:
                this.metrics.addOperator("!=");
                break;
            case ASTExpressionType.PostfixDecrement:
                this.metrics.addOperator("...--");
                break;
            case ASTExpressionType.PostfixIncrement:
                this.metrics.addOperator("...++");
                break;
            case ASTExpressionType.PrefixDecrement:
                this.metrics.addOperator("--...");
                break;
            case ASTExpressionType.PrefixIncrement:
                this.metrics.addOperator("++...");
                break;
            case ASTExpressionType.SignedShiftRight:
                this.metrics.addOperator(">>>");
                break;
            case ASTExpressionType.SignedShiftRightAssignment:
                this.metrics.addOperator(">>>=");
                break;
            case ASTExpressionType.SpreadOperator:
                this.metrics.addOperator("...");
                break;
            case ASTExpressionType.StrictEqual:
                this.metrics.addOperator("===");
                break;
            case ASTExpressionType.StrictNotEqual:
                this.metrics.addOperator("!==");
                break;
            case ASTExpressionType.Subtract:
                this.metrics.addOperator("-");
                break;
            case ASTExpressionType.SubtractAssignment:
                this.metrics.addOperator("-=");
                break;
            case ASTExpressionType.Sum:
                this.metrics.addOperator("+");
                break;
            case ASTExpressionType.SumAssignment:
                this.metrics.addOperator("+=");
                break;
            case ASTExpressionType.TernaryOperator:
                this.metrics.addOperator("... ? ... : ...");
                break;
            case ASTExpressionType.TypeofOperator:
                this.metrics.addOperator("typeof");
                break;
            case ASTExpressionType.UnaryMinus:
                this.metrics.addOperator("-...");
                break;
            case ASTExpressionType.UnaryPlus:
                this.metrics.addOperator("+...");
                break;
            case ASTExpressionType.VoidOperator:
                this.metrics.addOperator("void");
                break;
            case ASTExpressionType.YieldOperator:
                this.metrics.addOperator("yield");
                break;
        }
    }

    private calcASTLiteral(literal: ASTLiteral) {
        if (literal instanceof ASTNumberLiteral)
            return literal.value;
        if (literal instanceof ASTStringLiteral)
            return `\"${literal.value}\"`;
        if (literal instanceof ASTPrimitiveLiteral)
            return literal.keyword.keyword;
        return ""
    }

    private toStringFunctionCallName(node: ASTExpression): string {
        if (node.expressionType == ASTExpressionType.MemberAccess) {
            let left = this.toStringFunctionCallName(<ASTExpression>node.operands[0]);
            let right = this.toStringFunctionCallName(<ASTExpression>node.operands[1]);
            return left + "." + right;
        }
        if (node.expressionType == ASTExpressionType.Identifier) {
            let identifier = <ASTIdentifier>node.operands[0];
            return identifier.identifier;
        }
        return "";
    }
}