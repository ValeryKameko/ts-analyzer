import { Dictionary } from 'typescript-collections';
import { ASTNode, ASTProgram, ASTStatement, ASTVariableDeclarationStatement, ASTIfStatement, ASTExpression, ASTWhileStatement, ASTBlockStatement, ASTForStatement, ASTExpressionStatement, ASTReturnStatement, ASTSwitchStatement, ASTDefaultClause, ASTCaseClause, ASTFunctionDeclarationStatement, ASTDoWhileStatement, ASTExpressionType, ASTIdentifier, ASTFunctionLiteral, ASTFunctionCallParameters } from '../AST';
import { throws } from 'assert';
import { expectASTIdentifierExpression } from '../Parsers';

enum PaddingType {
    Center,
    Left
}

export class DataFlowComplexityMetrics {
    public variables: Set<string> = new Set();
    public used_variables: Set<string> = new Set();
    public control_variables: Set<string> = new Set();
    public modified_variables: Set<string> = new Set();
    public input_variables: Set<string> = new Set();
    public output_variables: Set<string> = new Set();
    public used_count: Map<string, number> = new Map();

    constructor() {
    }

    addVariable(variable: string) {
        this.used_count.set(variable, this.used_count.get(variable) || 0);
        this.variables.add(variable);
    }

    addUsedVariable(variable: string) {
        this.addVariable(variable);
        this.used_variables.add(variable);
        this.used_count.set(variable, (this.used_count.get(variable) || 0) + 1);
        console.log(`${variable} -- use variable`);

        this.used_count.set(variable, this.used_count.get(variable) || 0);
    }

    addControlVariable(variable: string) {
        this.addVariable(variable);
        this.control_variables.add(variable);
        console.log(`${variable} -- control variable`);
    }

    addModifiedVariable(variable: string) {
        this.addVariable(variable);
        this.modified_variables.add(variable);
        console.log(`${variable} -- modify variable`);
    }

    addInputVariable(variable: string) {
        this.addVariable(variable);
        this.input_variables.add(variable);
        console.log(`${variable} -- input variable`);
    }

    addOutputVariable(variable: string) {
        this.addVariable(variable);
        this.output_variables.add(variable);
        console.log(`${variable} -- output variable`);
    }

    toString(): string {
        const WIDTH = 50;

        let resultString = "";
        resultString += this.toTable(["Identifier", "Spen"], [...this.used_count.entries()]);
        let spenAll = 0;
        for (const [variable, count] of this.used_count.entries())
            spenAll += count;
        resultString += this.tableRow(["Summary spen", `${spenAll}`], WIDTH, PaddingType.Left);
        resultString += "\n\n\n";

        resultString += this.fullChepinMetrics() + '\n';
        resultString += "\n\n\n";
        resultString += this.ioChepinMetrics() + '\n';

        return resultString;
    }

    private fullChepinMetrics(): string {
        const WIDTH = 50;
        
        let resultString = "";

        resultString += this.tableSeparatorRow(1, WIDTH * 2 + 1, "-") + "\n";
        resultString += this.tableRow(["Full Chepin Metrics"], WIDTH * 2 + 1, PaddingType.Center) + "\n";
        resultString += this.tableSeparatorRow(6, WIDTH / 3, "-") + "\n";
        resultString += this.tableRow(["Group", "Variables", "Count"], Math.round(WIDTH * 2 / 3), PaddingType.Center) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), "-") + "\n";

        let full_p_variables: Set<string> = new Set<string>();
        let full_m_variables: Set<string> = new Set<string>();
        let full_c_variables: Set<string> = new Set<string>();
        let full_t_variables: Set<string> = new Set<string>();
        for (const variable of this.variables) {
            if (this.input_variables.has(variable) &&
                !this.control_variables.has(variable) &&
                !this.modified_variables.has(variable))
                full_p_variables.add(variable);
        }
        for (const variable of this.variables) {
            if (!this.control_variables.has(variable) &&
                this.modified_variables.has(variable))
                full_m_variables.add(variable);
        }
        for (const variable of this.variables) {
            if (this.control_variables.has(variable))
                full_c_variables.add(variable);
        }
        for (const variable of this.variables) {
            if (!this.used_variables.has(variable))
                full_t_variables.add(variable);
        }
        resultString += this.tableRow(
            ["P", [...full_p_variables].join(', '), [...full_p_variables].length.toString()],
            Math.round(WIDTH * 2 / 3),
            PaddingType.Left
        ) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), '-') + "\n";
        resultString += this.tableRow(
            ["M", [...full_m_variables].join(', '), [...full_m_variables].length.toString()],
            Math.round(WIDTH * 2 / 3),
            PaddingType.Left
        ) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), '-') + "\n";
        resultString += this.tableRow(
            ["C", [...full_c_variables].join(', '), [...full_c_variables].length.toString()],
            Math.round(WIDTH * 2 / 3),
            PaddingType.Left
        ) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), '-') + "\n";
        resultString += this.tableRow(
            ["T", [...full_t_variables].join(', '), [...full_t_variables].length.toString()],
            Math.round(WIDTH * 2 / 3),
            PaddingType.Left
        ) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), '-') + "\n";
        let q: number = 1.0 * [...full_p_variables].length +
            2.0 * [...full_m_variables].length +
            3.0 * [...full_c_variables].length +
            0.5 * [...full_t_variables].length;
        resultString += `Q = ${q}`;

        return resultString;
    }

    private ioChepinMetrics(): string {
        const WIDTH = 50;
        
        let resultString = "";

        resultString += this.tableSeparatorRow(1, WIDTH * 2 + 1, "-") + "\n";
        resultString += this.tableRow(["I/O Chepin Metrics"], WIDTH * 2 + 1, PaddingType.Center) + "\n";
        resultString += this.tableSeparatorRow(6, WIDTH / 3, "-") + "\n";
        resultString += this.tableRow(["Group", "Variables", "Count"], Math.round(WIDTH * 2 / 3), PaddingType.Center) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), "-") + "\n";
        let io_p_variables: Set<string> = new Set<string>();
        let io_m_variables: Set<string> = new Set<string>();
        let io_c_variables: Set<string> = new Set<string>();
        let io_t_variables: Set<string> = new Set<string>();
        for (const variable of this.variables) {
            if (this.input_variables.has(variable) &&
                !this.control_variables.has(variable) &&
                !this.modified_variables.has(variable))
                io_p_variables.add(variable);
        }
        for (const variable of this.variables) {
            if ((this.input_variables.has(variable) || this.output_variables.has(variable)) &&
                !this.control_variables.has(variable) &&
                this.modified_variables.has(variable))
                io_m_variables.add(variable);
        }
        for (const variable of this.variables) {
            if ((this.input_variables.has(variable) || this.output_variables.has(variable)) &&
                this.control_variables.has(variable))
                io_c_variables.add(variable);
        }
        for (const variable of this.variables) {
            if ((this.input_variables.has(variable) || this.output_variables.has(variable)) &&
                !this.used_variables.has(variable))
                io_t_variables.add(variable);
        }
        resultString += this.tableRow(
            ["P", [...io_p_variables].join(', '), [...io_p_variables].length.toString()],
            Math.round(WIDTH * 2 / 3),
            PaddingType.Left
        ) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), '-') + "\n";
        resultString += this.tableRow(
            ["M", [...io_m_variables].join(', '), [...io_m_variables].length.toString()],
            Math.round(WIDTH * 2 / 3),
            PaddingType.Left
        ) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), '-') + "\n";
        resultString += this.tableRow(
            ["C", [...io_c_variables].join(', '), [...io_c_variables].length.toString()],
            Math.round(WIDTH * 2 / 3),
            PaddingType.Left
        ) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), '-') + "\n";
        resultString += this.tableRow(
            ["T", [...io_t_variables].join(', '), [...io_t_variables].length.toString()],
            Math.round(WIDTH * 2 / 3),
            PaddingType.Left
        ) + "\n";
        resultString += this.tableSeparatorRow(3, Math.round(WIDTH * 2 / 3), '-') + "\n";
        let q: number = 1.0 * [...io_p_variables].length +
            2.0 * [...io_m_variables].length +
            3.0 * [...io_c_variables].length +
            0.5 * [...io_t_variables].length;
        resultString += `Q = ${q}`;

        return resultString;
    }

    private toTable(titles: string[], keyValues: [string, number][]): string {
        const WIDTH = 50;
        let table = "";
        table += this.tableSeparatorRow(2, WIDTH, "-") + "\n";
        table += this.tableRow(titles, WIDTH, PaddingType.Center) + "\n";
        table += this.tableSeparatorRow(2, WIDTH, "-") + "\n";
        for (const keyValue of keyValues) {
            table += this.tableRow([keyValue[0], Number(keyValue[1]).toString()], WIDTH, PaddingType.Left) + "\n";
            table += this.tableSeparatorRow(2, WIDTH, "-") + "\n";
        }
        return table;
    }

    private tableRow(values: string[], width: number, padding: PaddingType): string {
        let row = "";
        let slices = values.map(value => this.slicify(value, width));
        let row_count = slices.map(slice => slice.length).reduce((a, b) => Math.max(a, b));
        for (let i = 0; i < row_count; i++) {
            if (i != 0)
                row += '\n';
            row += "|";
            let row_values: string[] = [];
            for (const slice of slices)
                row_values.push(slice[i] || '');
            for (const value of row_values) {
                let padding_count = width - value.length - 1;
                if (padding == PaddingType.Center) {
                    let left_padding = Math.round(padding_count / 2);
                    let right_padding = padding_count - left_padding;
                    row += ' '.repeat(1 + left_padding) + value + ' '.repeat(right_padding);
                } else {
                    row += ' ' + value + ' '.repeat(padding_count);
                }
                row += "|";
            }
        }
        return row;
    }

    private slicify(str: string, width: number): string[] {
        let slices: string[] = [];
        let currString = str;
        while (currString != "") {
            let candidate = currString.substr(0, width);
            if (candidate.length == width) {
                let lastWhitespace = candidate.lastIndexOf(" ");
                if (lastWhitespace >= candidate.length / 5) {
                    candidate = candidate.substring(0, lastWhitespace + 1);
                }
            }
            slices.push(candidate);
            currString = currString.substr(candidate.length);
        }
        return slices;
    }

    private tableSeparatorRow(count: number, width: number, fillChar: string = ' '): string {
        let row = "+";
        for (let i = 0; i < count; i++) {
            row += fillChar.repeat(width);
            row += "+";
        }
        return row;
    }

}

enum VariableModifiers {
    Modified,
    Input,
    Output,
    Used,
    Control
}

export class DataFlowComplexityMetricsAnalyzer {
    private metrics: DataFlowComplexityMetrics = new DataFlowComplexityMetrics();
    constructor() {
    }

    calcMetrics(node: ASTNode): DataFlowComplexityMetrics {
        this.metrics = new DataFlowComplexityMetrics();
        this.calcASTNode(node);
        return this.metrics;
    }

    private calcASTNode(node: ASTNode) {
        if (node instanceof ASTProgram)
            return this.calcASTProgram(node);
    }

    private calcASTProgram(node: ASTProgram) {
        for (const statement of node.statements)
            this.calcASTStatement(<ASTStatement>statement);
    }

    private calcASTStatement(node: ASTStatement): void {
        if (node instanceof ASTFunctionDeclarationStatement)
            return this.calcASTStatement(node.body);

        if (node instanceof ASTIfStatement) {
            this.calcASTExpression(
                node.condition,
                new Set<VariableModifiers>([VariableModifiers.Control, VariableModifiers.Used]));

            for (const statement of node.ifStatements)
                this.calcASTStatement(statement);
            for (const statement of node.elseStatements)
                this.calcASTStatement(statement);
            return;
        }
        if (node instanceof ASTVariableDeclarationStatement) {
            for (const variableDeclaration of node.variableDeclarations) {
                this.metrics.addVariable(variableDeclaration.identifier.identifier);
                if (variableDeclaration.initializer)
                    this.calcASTExpression(
                        variableDeclaration.initializer,
                        new Set<VariableModifiers>([VariableModifiers.Used]));
            }
            return;
        }
        if (node instanceof ASTBlockStatement) {
            for (const statement of node.statements)
                this.calcASTStatement(statement);
            return;
        }
        if (node instanceof ASTWhileStatement) {
            this.calcASTExpression(
                node.condition,
                new Set<VariableModifiers>([VariableModifiers.Control, VariableModifiers.Used]));
            this.calcASTStatement(node.body);
        }
        if (node instanceof ASTForStatement) {
            if (node.initializer)
                this.calcASTStatement(node.initializer);
            if (node.condition)
                this.calcASTExpression(
                    node.condition,
                    new Set<VariableModifiers>([VariableModifiers.Control, VariableModifiers.Used]));
            if (node.afterExpression)
                this.calcASTExpression(
                    node.afterExpression,
                    new Set<VariableModifiers>([VariableModifiers.Used]));

            this.calcASTStatement(node.body);
            return;
        }
        if (node instanceof ASTReturnStatement) {
            if (node.expression)
                this.calcASTExpression(
                    node.expression,
                    new Set<VariableModifiers>([VariableModifiers.Used]));
            return;
        }
        if (node instanceof ASTExpressionStatement) {
            this.calcASTExpression(
                node.expression,
                new Set<VariableModifiers>([VariableModifiers.Used]));
            return;
        }
        if (node instanceof ASTSwitchStatement) {
            this.calcASTExpression(
                node.condition,
                new Set<VariableModifiers>([VariableModifiers.Control, VariableModifiers.Used]));
            for (const clause of node.clauses) {
                if (clause instanceof ASTDefaultClause) {
                    for (const statement of clause.statements)
                        this.calcASTStatement(statement);
                }
                if (clause instanceof ASTCaseClause) {
                    for (const statement of clause.statements)
                        this.calcASTStatement(statement);
                }
            }
            return;
        }
        if (node instanceof ASTDoWhileStatement) {
            this.calcASTExpression(
                node.condition,
                new Set<VariableModifiers>([VariableModifiers.Control, VariableModifiers.Used]));
            this.calcASTStatement(node.body);
            return;
        }
    }

    private calcASTExpression(node: ASTExpression, variable_modifiers: Set<VariableModifiers>): Set<VariableModifiers> {
        const assigmentOperators = [
            ASTExpressionType.Assignment,
            ASTExpressionType.SumAssignment,
            ASTExpressionType.SubtractAssignment,
            ASTExpressionType.MultiplyAssignment,
            ASTExpressionType.SignedShiftRightAssignment,
            ASTExpressionType.BitwiseXorAssignment,
            ASTExpressionType.BitwiseAndAssignment,
            ASTExpressionType.BitwiseOrAssignment,
            ASTExpressionType.DivisionAssignment,
            ASTExpressionType.LogicalShiftLeftAssignment,
            ASTExpressionType.LogicalShiftRightAssignment,
            ASTExpressionType.ModuloAssignment,
            ASTExpressionType.PowerAssignment,
        ];

        if (![
            ASTExpressionType.FunctionCall,
            ASTExpressionType.Literal,
            ASTExpressionType.Identifier,
        ].includes(node.expressionType)) {
            let additionalModifiers = new Set<VariableModifiers>();
            for (let i = 1; i < node.operands.length; i++) {
                let operand = <ASTExpression>node.operands[i];
                additionalModifiers = new Set<VariableModifiers>([
                    ...additionalModifiers,
                    ...this.calcASTExpression(operand, variable_modifiers)
                ]);
            }
            variable_modifiers = new Set<VariableModifiers>([
                ...variable_modifiers,
                ...additionalModifiers
            ]);

            if (assigmentOperators.includes(node.expressionType)) {
                this.calcASTExpression(
                    <ASTExpression>node.operands[0],
                    new Set<VariableModifiers>([
                        ...variable_modifiers,
                        VariableModifiers.Modified
                    ]));
            } else {
                this.calcASTExpression(
                    <ASTExpression>node.operands[0],
                    variable_modifiers);
            }
            return additionalModifiers;
        }
        switch (node.expressionType) {
            case ASTExpressionType.Identifier:
                let identifier = (<ASTIdentifier>node.operands[0]).identifier;
                this.addVariable(identifier, variable_modifiers);
                break;
            case ASTExpressionType.FunctionCall:
                const input_functions = ['read', 'input', 'scan'];
                const output_functions = ['console', 'log', 'write', 'print', 'println'];

                let functionCallParameters = <ASTFunctionCallParameters>node.operands[1];
                let functionCallName = this.toStringFunctionCallName(<ASTExpression>node.operands[0]);

                let additionalModifiers = new Set<VariableModifiers>();
                if (input_functions.some(value => functionCallName.search(value) != -1)) {
                    additionalModifiers = new Set<VariableModifiers>([
                        ...additionalModifiers,
                        VariableModifiers.Input]);
                    console.log('///input function');
                } else if (output_functions.some(value => functionCallName.search(value) != -1)) {
                    additionalModifiers = new Set<VariableModifiers>([
                        ...additionalModifiers,
                        VariableModifiers.Output]);
                    console.log('///output function');
                }
                let newModifiers = new Set<VariableModifiers>();
                for (const parameter of functionCallParameters.parameters) {
                    newModifiers = new Set<VariableModifiers>([
                        ...newModifiers,
                        ...this.calcASTExpression(parameter, new Set<VariableModifiers>([
                            ...variable_modifiers,
                            ...additionalModifiers
                        ])
                        )]);
                }
                return new Set<VariableModifiers>([...additionalModifiers, ...newModifiers]);
        }
        return new Set<VariableModifiers>([]);
    }

    private toStringFunctionCallName(node: ASTExpression): string {
        if (node.expressionType == ASTExpressionType.MemberAccess) {
            let left = this.toStringFunctionCallName(<ASTExpression>node.operands[0]);
            let right = this.toStringFunctionCallName(<ASTExpression>node.operands[1]);
            return left + "." + right;
        }
        if (node.expressionType == ASTExpressionType.Identifier) {
            let identifier = <ASTIdentifier>node.operands[0];
            return identifier.identifier;
        }
        return "";
    }

    private addVariable(variable: string, modifiers: Set<VariableModifiers>) {
        this.metrics.addVariable(variable);
        if (modifiers.has(VariableModifiers.Input))
            this.metrics.addInputVariable(variable);
        if (modifiers.has(VariableModifiers.Control))
            this.metrics.addControlVariable(variable);
        if (modifiers.has(VariableModifiers.Output))
            this.metrics.addOutputVariable(variable);
        if (modifiers.has(VariableModifiers.Used))
            this.metrics.addUsedVariable(variable);
        if (modifiers.has(VariableModifiers.Modified))
            this.metrics.addModifiedVariable(variable);
        console.log('');
    }
}