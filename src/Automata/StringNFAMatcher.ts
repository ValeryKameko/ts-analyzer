import { StringNFAState, StringNFATransition } from "./StringNFA";
import { NFAMatcher } from "./NFAMatcher";

export class StringNFAMatcher extends NFAMatcher<StringNFAState, StringNFATransition> {
    public matchString(s: string): boolean {
        return this.match(this.stringToTransitions(s));
    }
    public firstMatchString(s: string) {
        return this.firstMatch(this.stringToTransitions(s))
    }

    public lastMatchString(s: string) {
        return this.lastMatch(this.stringToTransitions(s));
    }
    private stringToTransitions(s: string): StringNFATransition[] {
        let transitions: StringNFATransition[] = [];
        for (const char of s)
            transitions.push(new StringNFATransition(char));
        return transitions;
    }
}