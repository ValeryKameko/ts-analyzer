export interface IDFAState {
}

export interface IDFATransition {
}

export interface DFAInterface<State extends IDFAState, Transition extends IDFATransition> {
    addState(state: State): void;
    removeState(state: State): void;

    hasTransition(fromState: State, transition: Transition): boolean;
    hasTranstitionTo(fromState: State, toState: State): boolean;

    addTransition(fromState: State, toState: State, transition: Transition): void;

    deleteTransition(fromState: State, transition: Transition): void;
    deleteTransitionTo(fromState: State, toState: State): void;

    getTransition(fromState: State, filterTransition: ((transition: Transition) => boolean)): Transition | undefined;
    getTransitions(fromState: State): Transition[];
}

// class DFA<State, Transition> implements DFAInterface<State, Transition> {

// }