import { Set, Dictionary, MultiDictionary } from "typescript-collections";

export class IdCounter {
    protected _id: number;
    constructor() {
        this._id = 0;
    }
    public nextId() {
        return this._id++;
    }
}

export abstract class AbstractIdCounted {
    private static _counter: IdCounter = new IdCounter();
    private _id: number;
    constructor() {
        this._id = AbstractIdCounted._counter.nextId();
    }
    public get id() {
        return this._id;
    }
}

export abstract class AbstractNFAState extends AbstractIdCounted {
    name?: string;
    constructor() {
        super();
    }
    toString(): string {
        return this.id.toString();
    }
}

export abstract class AbstractNFATransition {
    constructor() {
    }
    public abstract toString(): string;
}

export interface NFAInterface<State extends AbstractNFAState, Transition extends AbstractNFATransition> {
    hasState(state: State): boolean;
    addState(state: State): void;
    removeState(state: State): void;
    getStates(): State[];

    hasTransitions(fromState: State, transition: Transition): boolean;
    hasTranstitionsTo(fromState: State, toState: State): boolean;

    addTransition(fromState: State, toState: State, transition: Transition): void;
    getTransitionsTo(fromState: State, toState: State): Transition[];
    getTransitions(fromState: State): Dictionary<Transition, Set<State>>;

    deleteTransition(fromState: State, transition: Transition): void;
    deleteTransitionsTo(fromState: State, toState: State): void;

    isInitialState(state: State): boolean;
    setInitialState(state: State): void;
    unsetInitialState(state: State): void;
    getInitialStates(): State[];

    isEndState(state: State): boolean;
    setEndState(state: State): void;
    unsetEndState(state: State): void;
    getEndStates(): State[];

    addNFA(nfa: NFAInterface<State, Transition>): void;
    getEpsilonTransition(): Transition;
}

export class NFA<State extends AbstractNFAState, Transition extends AbstractNFATransition> implements NFAInterface<State, Transition> {
    private _states: Set<State>;
    private _arrows: Dictionary<State, Dictionary<Transition, Set<State>>>;
    private _initialStates: Set<State>;
    private _endStates: Set<State>;
    protected _epsilonTransition: Transition | undefined;

    constructor() {
        this._states = new Set();
        this._initialStates = new Set();
        this._endStates = new Set();
        this._arrows = new Dictionary();
    }
    hasState(state: State) {
        return this._states.contains(state);
    }
    addState(state: State) {
        this._states.add(state);
    }
    removeState(state: State) {
        this._states.remove(state);
        this._initialStates.remove(state);
        this._endStates.remove(state);
        this._arrows.remove(state);
        this._arrows.forEach((stateKey, stateValue) => {
            stateValue.forEach((transitionKey, transitionValue) => {
                transitionValue.remove(state);
            });
        });
    }
    getStates(): State[] {
        return this._states.toArray();
    }
    hasTransitions(fromState: State, transition: Transition): boolean {
        let transitions = this._arrows.getValue(fromState);
        if (!transitions)
            return false;
        let adjacentStates = transitions.getValue(transition);
        if (!adjacentStates)
            return false;
        return adjacentStates.isEmpty();
    }
    hasTranstitionsTo(fromState: State, toState: State): boolean {
        let transitions = this._arrows.getValue(fromState);
        if (!transitions)
            return false;
        let foundIndex = transitions.values().findIndex((value, index, obj) => {
            return value.contains(toState);
        });
        return foundIndex !== -1;
    }
    addTransition(fromState: State, toState: State, transition: Transition) {
        this.checkState(fromState);
        this.checkState(toState);
        let transitions = this._arrows.getValue(fromState);
        if (!transitions) {
            transitions = new Dictionary<Transition, Set<State>>();
            this._arrows.setValue(fromState, transitions);
        }
        let adjacentStates = transitions.getValue(transition);
        if (!adjacentStates) {
            let stateSet = new Set<State>();
            stateSet.add(toState);
            transitions.setValue(transition, stateSet);
            return;
        }
        adjacentStates.add(toState);
    }
    getTransitionsTo(fromState: State, toState: State): Transition[] {
        let transitions = this._arrows.getValue(fromState);
        if (!transitions)
            throw new Error(`No transitions found for ${fromState.name || fromState.id}`);
        let answer: Transition[] = [];
        transitions.forEach((key, values) => {
            values.forEach((value) => {
                if (value.toString() === toState.toString())
                    answer.push(key);
            });
        });
        return answer;
    }
    getTransitions(fromState: State): Dictionary<Transition, Set<State>> {
        let transitions = this._arrows.getValue(fromState);
        if (!transitions) {
            transitions = new Dictionary<Transition, Set<State>>()
            this._arrows.setValue(fromState, transitions);
            return transitions;
        }
        return transitions;
    }
    deleteTransition(fromState: State, transition: Transition) {
        this._arrows.forEach((key, value) => {
            value.remove(transition);
        });
    }
    deleteTransitionsTo(fromState: State, toState: State) {
        let transitions = this._arrows.getValue(fromState);
        if (!transitions)
            throw new Error(`NFA has no state ${fromState.name || fromState.id}`);
        transitions.forEach((key, value) => {
            value.remove(toState);
        });
    }
    isInitialState(state: State): boolean {
        return this._initialStates.contains(state);
    }
    setInitialState(state: State) {
        this._initialStates.add(state);
    }
    unsetInitialState(state: State) {
        this._initialStates.remove(state);
    }
    getInitialStates(): State[] {
        return this._initialStates.toArray();
    }
    isEndState(state: State): boolean {
        return this._endStates.contains(state);
    }
    setEndState(state: State) {
        this._endStates.add(state);
    }
    unsetEndState(state: State) {
        this._endStates.remove(state);
    }
    getEndStates(): State[] {
        return this._endStates.toArray();
    }

    addNFA(nfa: NFAInterface<State, Transition>) {
        for (const state of nfa.getStates()) {
            this.addState(state);
            if (nfa.isEndState(state))
                this.setEndState(state);
            if (nfa.isInitialState(state))
                this.setInitialState(state);
        }
        for (const fromState of nfa.getStates()) {
            nfa.getTransitions(fromState).forEach((transition, toStates) => {
                toStates.forEach((toState) => {
                    this.addTransition(fromState, toState, transition);
                });
            });
        }
        for (const fromState of nfa.getStates()) {
            nfa.getTransitions(fromState).forEach((transition) => {
                nfa.deleteTransition(fromState, transition);
            });
        }
        for (const state of nfa.getStates()) {
            nfa.removeState(state);
        }
    }
    getEpsilonTransition(): Transition {
        throw new Error('getEpsilonTransition() not implemented');
    }

    private checkState(state: State) {
        if (!this.hasState(state))
            throw new Error(`NFA has no state ${state.name || state.id}`);
    }

}