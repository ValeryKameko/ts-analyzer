import {} from "./NFA";
import {} from "./NFAIterator";
import { StringNFA, StringNFAState, StringNFATransition } from "./StringNFA";
import { normalizeEndsStringNFA, concatStringNFA, concatModifyStringNFA, 
    multiplyModifyStringNFA, maybeModifyStringNFA, orModifyStringNFA } from "./StringNFAOperations";

export class StringNFACreator {
    private _value: string;
    private _index: number;
    constructor(value: string) {
        this._value = value;
        this._index = 0;
    }
    create(): StringNFA {
        let nfa = this.makeSequence('');
        return nfa;
    }

    private makeSequence(breakSet: string): StringNFA {
        let nfa = this.createNFA();
        let lastNFA = this.createNFA();
        let rawInterpretation = false;
        while ((this._index < this._value.length) && !breakSet.includes(this._value[this._index])) {
            let char = this._value[this._index];
            if (breakSet.includes(char) && !rawInterpretation) {
                concatModifyStringNFA(nfa, lastNFA);
                break;
            } else if (char === '[' && !rawInterpretation) {
                rawInterpretation = false;
                let newNFA = this.makeSet();
                concatModifyStringNFA(nfa, lastNFA);
                lastNFA = newNFA;
            } else if (char === '(' && !rawInterpretation) {
                rawInterpretation = false;
                let newNFA = this.makeChoice();
                concatModifyStringNFA(nfa, lastNFA);
                lastNFA = newNFA;
            } else if (char === '*' && !rawInterpretation) {
                rawInterpretation = false;
                multiplyModifyStringNFA(lastNFA);
                this._index++;
            } else if (char === '?' && !rawInterpretation) {
                rawInterpretation = false;
                maybeModifyStringNFA(lastNFA);
                this._index++;
            } else if (char === '\\' && !rawInterpretation) {
                rawInterpretation = true;
                this._index++;
            } else {
                rawInterpretation = false;
                normalizeEndsStringNFA(nfa);
                concatModifyStringNFA(nfa, lastNFA);
                lastNFA = this.createNFA();
                this.appendTransition(lastNFA, new StringNFATransition(char));
                this._index++;
            }
        }
        concatModifyStringNFA(nfa, lastNFA);
        return nfa;
    }
    private makeChoice(): StringNFA {
        let nfa = new StringNFA();
        while (this._index < this._value.length && this._value[this._index] !== ')') {
            this._index++;
            let lastNFA = this.makeSequence('|)');
            orModifyStringNFA(nfa, lastNFA);
        }
        this._index++;
        return nfa;
    }
    private makeSet() {
        let nfa = new StringNFA();
        let initialState = new StringNFAState();
        let endState = new StringNFAState();
        [initialState, endState].forEach(state => {
            nfa.addState(state);
        });
        nfa.setInitialState(initialState);
        nfa.setEndState(endState);
        this._index++;
        while (this._index < this._value.length && this._value[this._index] !== ']') {
            let char = this._value[this._index];
            nfa.addTransition(initialState, endState, new StringNFATransition(char));
            this._index++;
        }
        this._index++;
        return nfa;
    }
    private appendTransition(nfa: StringNFA, transition: StringNFATransition) {
        normalizeEndsStringNFA(nfa);
        let newEnd = new StringNFAState();
        nfa.addState(newEnd);
        nfa.addTransition(nfa.getEndStates()[0], newEnd, transition);
        for (const state of nfa.getEndStates())
            nfa.unsetEndState(state);
        nfa.setEndState(newEnd);
    }
    private createNFA() {
        let nfa = new StringNFA();
        let initialState = new StringNFAState();
        nfa.addState(initialState);
        nfa.setInitialState(initialState);
        nfa.setEndState(initialState);
        return nfa;
    }
}
