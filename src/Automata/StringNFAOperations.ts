import * as NFA from "./NFA";
import * as NFAIterator from "./NFAIterator";
import { Dictionary } from "typescript-collections";
import { StringNFA, StringNFAState, StringNFATransition } from "./StringNFA";
// import { forEach } from "typescript-collections/dist/lib/arrays";


export function copyStringNFA(oldNFA: StringNFA): StringNFA {
    let newState = new Dictionary<StringNFAState, StringNFAState>();
    let copyNFA: StringNFA = new StringNFA();
    for (const state of oldNFA.getStates()) {
        let copyState = new StringNFAState();
        newState.setValue(state, copyState);
    }
    newState.forEach((oldState, newState) => {
        copyNFA.addState(newState);
    });
    for (const oldState of oldNFA.getInitialStates()) {
        let copyState = newState.getValue(oldState);
        if (copyState)
            copyNFA.setInitialState(copyState);
    }
    for (const oldState of oldNFA.getEndStates()) {
        let copyState = newState.getValue(oldState);
        if (copyState)
            copyNFA.setEndState(copyState);
    }

    newState.forEach((oldFromState, newFromState) => {
        oldNFA.getTransitions(oldFromState).forEach((transition, toStates) => {
            toStates.forEach((toState) => {
                let copyToState = newState.getValue(toState);
                if (copyToState) {
                    let newTransition = new StringNFATransition(transition.label);
                    copyNFA.addTransition(newFromState, copyToState, newTransition);
                }
            });
        });
    });
    return copyNFA;
}

export function orStringNFA(nfa1: StringNFA, nfa2: StringNFA): StringNFA {
    let orNFA = copyStringNFA(nfa1); 
    orNFA.addNFA(copyStringNFA(nfa2));
    return orNFA;
}

export function concatStringNFA(nfa1: StringNFA, nfa2: StringNFA): StringNFA {
    let concatNFA = copyStringNFA(nfa1);
    let joinState = new StringNFAState();
    concatNFA.addState(joinState);
    for (const state of concatNFA.getEndStates()) {
        concatNFA.unsetEndState(state);
        concatNFA.addTransition(state, joinState, new StringNFATransition(''));
    }
    let savedInitialStates = nfa2.getInitialStates();
    concatNFA.addNFA(nfa2);
    for (const state of savedInitialStates) {
        concatNFA.unsetInitialState(state);
        concatNFA.addTransition(joinState, state, new StringNFATransition(''));
    }
    return concatNFA;
}

export function multiplyStringNFA(nfa: StringNFA): StringNFA {
    let copy = copyStringNFA(nfa);
    let newInitialState = new StringNFAState();
    let newNextInitialState = new StringNFAState();
    let newPrevEndState = new StringNFAState();
    let newEndState = new StringNFAState();
    for (const state of [newInitialState, newNextInitialState, newPrevEndState, newEndState]) {
        copy.addState(state);
    }
    for (const state of copy.getInitialStates()) {
        copy.unsetInitialState(state);
        copy.addTransition(newNextInitialState, state, new StringNFATransition(''));
    }
    copy.addTransition(newInitialState, newNextInitialState, new StringNFATransition(''));
    for (const state of copy.getEndStates()) {
        copy.unsetEndState(state);
        copy.addTransition(state, newPrevEndState, new StringNFATransition(''));
    }
    copy.addTransition(newPrevEndState, newEndState, new StringNFATransition(''));
    copy.addTransition(newInitialState, newEndState, new StringNFATransition(''));
    copy.addTransition(newEndState, newNextInitialState, new StringNFATransition(''));
    copy.setInitialState(newInitialState);
    copy.setEndState(newEndState);
    return copy;
}

export function skipStringNFA(nfa: StringNFA): StringNFA {
    let copy = copyStringNFA(nfa);
    let newInitialState = new StringNFAState();
    let newEndState = new StringNFAState();
    [newInitialState, newEndState].forEach((state) => {
        copy.addState(state);
    });
    for (const state of copy.getInitialStates()) {
        copy.unsetInitialState(state);
        copy.addTransition(newInitialState, state, new StringNFATransition(''));
    }
    copy.setInitialState(newInitialState);

    for (const state of copy.getEndStates()) {
        copy.unsetEndState(state);
        copy.addTransition(state, newEndState, new StringNFATransition(''));
    }
    copy.setEndState(newEndState);

    copy.addTransition(newInitialState, newEndState, new StringNFATransition(''));
    return copy;
}

export function orModifyStringNFA(nfa1: StringNFA, nfa2: StringNFA): void { 
    nfa1.addNFA(nfa2);
}

export function concatModifyStringNFA(nfa1: StringNFA, nfa2: StringNFA): void {
    let joinState: StringNFAState;
    if (nfa1.getEndStates().length != 1) {
        joinState = new StringNFAState();
        nfa1.addState(joinState);
        for (const state of nfa1.getEndStates()) {
            nfa1.unsetEndState(state);
            nfa1.addTransition(state, joinState, new StringNFATransition(''));
        }
    } else {
        joinState = nfa1.getEndStates()[0];
        for (const state of nfa1.getEndStates())
            nfa1.unsetEndState(state);
    }
    let savedInitialStates = nfa2.getInitialStates();
    nfa1.addNFA(nfa2);
    for (const state of savedInitialStates) {
        nfa1.unsetInitialState(state);
        nfa1.addTransition(joinState, state, new StringNFATransition(''));
    }
}

export function multiplyModifyStringNFA(nfa: StringNFA): void {
    let newInitialState = new StringNFAState();
    let newNextInitialState = new StringNFAState();
    let newPrevEndState = new StringNFAState();
    let newEndState = new StringNFAState();
    for (const state of [newInitialState, newNextInitialState, newPrevEndState, newEndState]) { 
        nfa.addState(state);
    }
    for (const state of nfa.getInitialStates()) {
        nfa.unsetInitialState(state);
        nfa.addTransition(newNextInitialState, state, new StringNFATransition(''));
    }
    nfa.addTransition(newInitialState, newNextInitialState, new StringNFATransition(''));
    for (const state of nfa.getEndStates()) {
        nfa.unsetEndState(state);
        nfa.addTransition(state, newPrevEndState, new StringNFATransition(''));
    }
    nfa.addTransition(newPrevEndState, newEndState, new StringNFATransition(''));
    nfa.addTransition(newInitialState, newEndState, new StringNFATransition(''));
    nfa.addTransition(newEndState, newNextInitialState, new StringNFATransition(''));
    nfa.setInitialState(newInitialState);
    nfa.setEndState(newEndState);
}

export function normalizeEndsStringNFA(nfa: StringNFA): void {
    if (nfa.getEndStates().length <= 1)
        return;
    let newEndState = new StringNFAState();
    nfa.addState(newEndState);
    for (const state of nfa.getEndStates()) {
        nfa.unsetEndState(state);
        nfa.addTransition(state, newEndState, new StringNFATransition(''));
    }
    nfa.setEndState(newEndState);
}

export function normalizeInitialsStringNFA(nfa: StringNFA): void {
    if (nfa.getInitialStates().length <= 1)
        return;
    let newInitialState = new StringNFAState();
    nfa.addState(newInitialState);
    for (const state of nfa.getInitialStates()) {
        nfa.unsetInitialState(state);
        nfa.addTransition(newInitialState, state, new StringNFATransition(''));
    }
    nfa.setInitialState(newInitialState);
}

export function maybeModifyStringNFA(nfa: StringNFA): void {
    normalizeInitialsStringNFA(nfa);
    normalizeEndsStringNFA(nfa);
    nfa.addTransition(nfa.getInitialStates()[0], nfa.getEndStates()[0], new StringNFATransition(''));
}