import * as NFA from "./NFA";
import { Set } from "typescript-collections";
import { StringNFATransition } from "./StringNFA";

export class NFAIterator<State extends NFA.AbstractNFAState, Transition extends NFA.AbstractNFATransition> {
    private _nfa: NFA.NFAInterface<State, Transition>;
    private _currentStates: Set<State>;
    constructor(nfa: NFA.NFAInterface<State, Transition>) {
        this._nfa = nfa;
        this._currentStates = new Set();
    }
    public set currentStates(states: State[]) {
        this._currentStates.clear();
        for (const state of states) {
            this._currentStates.add(state);
        }
        this.skipEpsilonStates();
    }
    public get currentStates() {
        return this._currentStates.toArray();
    }
    public getCopy() {
        let copy = new NFAIterator(this._nfa);
        copy._currentStates = new Set();
        this._currentStates.forEach((state) => {
            copy._currentStates.add(state);
        });
        copy._nfa = this._nfa;
        return copy;

    }
    public getEndStates(): State[] {
        return this.currentStates.filter((state) => {
            return this._nfa.isEndState(state);
        });
    }
    public reset() {
        this._currentStates.clear();
        for (const state of this._nfa.getInitialStates()) {
            this._currentStates.add(state);
        }
        this.skipEpsilonStates();
    }
    public step(transition: Transition): NFAIterator<State, Transition> {
        let nextStates = new Set<State>();
        this.skipEpsilonStates();
        this._currentStates.forEach((fromState) => {
            this._nfa.getTransitions(fromState).forEach((toTransition, toState) => {
                if (toTransition.toString() === transition.toString())
                    nextStates.union(toState);
            });
        });
        this._currentStates = nextStates;
        this.skipEpsilonStates();
        return this;
    }
    private skipEpsilonStates() {
        let unseenStates: State[] = [];
        this._currentStates.forEach((state) => {
            unseenStates.push(state);
        });
        let currentState: State | undefined;
        while (currentState = unseenStates.pop()) {
            let transitions = this._nfa.getTransitions(currentState).getValue(this._nfa.getEpsilonTransition());
            if (transitions) {
                transitions.forEach((state) => {
                    if (!this._currentStates.contains(state)) {
                        this._currentStates.add(state);
                        unseenStates.push(state);
                    }
                });
            }
        }
    }
};