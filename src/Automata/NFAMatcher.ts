import { NFA, AbstractNFAState, AbstractNFATransition } from "./NFA";
import { NFAIterator } from "./NFAIterator";


export class NFAMatcher<State extends AbstractNFAState, Transition extends AbstractNFATransition> {
    constructor(public nfa: NFA<State, Transition>) {
    }
    public firstMatch(s: Transition[]): number | undefined {
        let pos = 0;
        let it = new NFAIterator(this.nfa);
        it.reset();
        while (it.getEndStates().length == 0 && pos < s.length) {
            it.step(s[pos]);
            pos++;
        }
        if (it.getEndStates().length == 0)
            return undefined;
        else
            return pos;
    }
    public lastMatch(s: Transition[]): number | undefined {
        let pos = 0;
        let answer: number | undefined = undefined;
        let it = new NFAIterator(this.nfa);
        it.reset();
        while (pos < s.length) {
            it.step(s[pos]);
            if (it.getEndStates().length != 0)
                answer = pos;
            pos++;
        }
        return answer;
    }
    public match(s: Transition[]): boolean {
        let pos = 0;
        let it = new NFAIterator(this.nfa);
        it.reset();
        for (const elem of s)
            it.step(elem);
        return it.getEndStates().length > 0;
    }
}