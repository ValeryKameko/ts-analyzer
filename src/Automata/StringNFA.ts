import * as NFA from "./NFA";
import { Dictionary } from "typescript-collections";

export class StringNFATransition extends NFA.AbstractNFATransition {
    constructor(public label: string) {
        super();
    }
    toString() {
        if (this.label !== "")
            return this.label;
        else
            return "epsilon";
    }
}

export class StringNFAState extends NFA.AbstractNFAState {
}

export class StringNFA extends NFA.NFA<StringNFAState, StringNFATransition> {
    getEpsilonTransition(): StringNFATransition {
        return new StringNFATransition('');
    }
}