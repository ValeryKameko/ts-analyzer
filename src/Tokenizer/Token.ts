
export enum TokenType {
    Keyword,
    
    Identifier,
    StringLiteral,
    NumericLiteral,

    Whitespaces,
    LineTerminatorSequence,

    Comment,
    Punctuator,

    Unknown,
    None
};

export class Token {
    constructor(
        public content: string,
        public type: TokenType
    ) {

    }
    valueOf() {
        return {
            content: this.content,
            type: TokenType[this.type]
        }.toString();
    }
}

export const alphabet = "abcdefghijklmnopqrstuvwxyz";
export const digits = "0123456789";