import {
    TokenizerContext, Token, TokenType, alphabet, digits
} from ".";

export class TokenExpectorContext {
    constructor(
        public tokenizerContext: TokenizerContext,
        public token: Token,
    ) {

    }
}

export type TokenExpector = (context: TokenizerContext) => TokenExpectorContext | undefined;

function expectKeywordToken(
    expectedKeyword: string,
    tokenType: TokenType,
    context: TokenizerContext
): TokenExpectorContext | undefined {
    let newContext = expectExpectors([
        expectChars(expectedKeyword),
        expectForward(
            expectExcept(
                expectCharOneOf(alphabet + digits + alphabet.toUpperCase() + "$" + "_")
            )
        )
    ])(context);
    if (newContext) {
        newContext.token = new Token(expectedKeyword, tokenType);
        return newContext;
    } else
        return undefined;
}

export const expectUnknownToken: TokenExpector = context => {
    let copy = context.getCopy();
    let lexeme = copy.iterator.getSymbol();
    if (lexeme)
        return new TokenExpectorContext(copy, new Token(lexeme, TokenType.Unknown));
    else
        return undefined;
}

export const keywordExpectors: TokenExpector[] = [
    context => expectKeywordToken("if", TokenType.Keyword, context),
    context => expectKeywordToken("else", TokenType.Keyword, context),
    context => expectKeywordToken("for", TokenType.Keyword, context),
    context => expectKeywordToken("while", TokenType.Keyword, context),
    context => expectKeywordToken("do", TokenType.Keyword, context),
    context => expectKeywordToken("const", TokenType.Keyword, context),
    context => expectKeywordToken("var", TokenType.Keyword, context),
    context => expectKeywordToken("let", TokenType.Keyword, context),
    context => expectKeywordToken("class", TokenType.Keyword, context),
    context => expectKeywordToken("interface", TokenType.Keyword, context),
    context => expectKeywordToken("static", TokenType.Keyword, context),
    context => expectKeywordToken("public", TokenType.Keyword, context),
    context => expectKeywordToken("private", TokenType.Keyword, context),
    context => expectKeywordToken("protected", TokenType.Keyword, context),
    context => expectKeywordToken("function", TokenType.Keyword, context),
    context => expectKeywordToken("constructor", TokenType.Keyword, context),
    context => expectKeywordToken("return", TokenType.Keyword, context),
    context => expectKeywordToken("new", TokenType.Keyword, context),
    context => expectKeywordToken("implements", TokenType.Keyword, context),
    context => expectKeywordToken("extends", TokenType.Keyword, context),
    context => expectKeywordToken("in", TokenType.Keyword, context),
    context => expectKeywordToken("of", TokenType.Keyword, context),
    context => expectKeywordToken("this", TokenType.Keyword, context),
    context => expectKeywordToken("number", TokenType.Keyword, context),
    context => expectKeywordToken("any", TokenType.Keyword, context),
    context => expectKeywordToken("string", TokenType.Keyword, context),
    context => expectKeywordToken("undefined", TokenType.Keyword, context),
    context => expectKeywordToken("null", TokenType.Keyword, context),
    context => expectKeywordToken("true", TokenType.Keyword, context),
    context => expectKeywordToken("false", TokenType.Keyword, context),
    context => expectKeywordToken("case", TokenType.Keyword, context),
    context => expectKeywordToken("default", TokenType.Keyword, context),
    context => expectKeywordToken("switch", TokenType.Keyword, context),
    context => expectKeywordToken("break", TokenType.Keyword, context),
    context => expectKeywordToken("continue", TokenType.Keyword, context),
    context => expectKeywordToken("import", TokenType.Keyword, context),
    context => expectKeywordToken("as", TokenType.Keyword, context),
    context => expectKeywordToken("from", TokenType.Keyword, context),
];

export const expectKeyword: TokenExpector =
    context => expectExpectorsOneOf(keywordExpectors)(context);

function expectCharOneOf(chars: string): TokenExpector {
    return context => {
        let copyContext = context.getCopy();
        let symbol: string | undefined;
        if (symbol = copyContext.iterator.getSymbol())
            if (chars.includes(symbol))
                return new TokenExpectorContext(copyContext, new Token(symbol, TokenType.None))
        return undefined;
    }
}

function expectChars(chars: string): TokenExpector {
    return context => expectToken(new Token(chars, TokenType.None))(context);
}

function expectToken(token: Token): TokenExpector {
    return context => {
        let copyContext = context.getCopy();
        if (copyContext.iterator.getSymbols(token.content.length) === token.content)
            return new TokenExpectorContext(copyContext, new Token(token.content, token.type));
        return undefined;
    }
}

function expectTokenOneOf(tokens: Token[]): TokenExpector {
    let expectors = tokens.map((token) => {
        return expectToken(token);
    });
    return expectExpectorsOneOf(expectors);
}

function expectTokens(tokens: Token[]): TokenExpector {
    let expectors = tokens.map(token => {
        return expectToken(token);
    });
    return expectExpectors(expectors);
}

function expectExpectorsOneOf(expectors: TokenExpector[]): TokenExpector {
    return context => {
        for (const expector of expectors) {
            let newContext = expector(context);
            if (newContext)
                return newContext;
        }
        return undefined;
    };
}

function expectExpectors(expectors: TokenExpector[]): TokenExpector {
    return context => {
        let newContext = new TokenExpectorContext(context, new Token('', TokenType.None));

        for (const expector of expectors) {
            let tempContext: TokenExpectorContext | undefined;
            if (tempContext = expector(newContext.tokenizerContext)) {
                newContext.token.content = newContext.token.content + tempContext.token.content;
                newContext.tokenizerContext = tempContext.tokenizerContext;
            } else
                return undefined;
        }
        return newContext;
    };
}

function expectMaybeExpector(expector: TokenExpector): TokenExpector {
    return context => {
        let newContext = expector(context);
        if (newContext)
            return newContext;
        else
            return new TokenExpectorContext(context, new Token('', TokenType.None));
    };
}

export const expectIdentifier: TokenExpector = context => {
    const identifierStartChars = alphabet + alphabet.toUpperCase() + "$" + "_";
    const identifierEndChars = identifierStartChars + digits;
    let newContext = expectExpectors([
        expectCharOneOf(identifierStartChars),
        expectExpectorsAnyCount(expectCharOneOf(identifierEndChars))
    ])(context);
    if (newContext) {
        newContext.token.type = TokenType.Identifier;
        return newContext;
    }
    return undefined;
}

export const expectStringLiteral: TokenExpector = context => {
    let stringLiteralExpectors: TokenExpector[] = [];
    for (const bracket of ['\'', '\"', '`']) {
        stringLiteralExpectors.push(
            expectExpectors([
                expectChars(bracket),
                expectExpectorsAnyCount(
                    expectExpectorsOneOf([
                        expectExcept(expectChars(bracket))
                    ])
                ),
                expectChars(bracket),
            ])
        );
    }
    let newContext =
        expectExpectorsOneOf(stringLiteralExpectors)(context);
    if (newContext) {
        newContext.token.type = TokenType.StringLiteral;
        return newContext;
    }
    return undefined;
}

export const expectNothing: TokenExpector = context => {
    return new TokenExpectorContext(context, new Token('', TokenType.None));
}

const expectExponentPart: TokenExpector = context => {
    return expectExpectors([
        expectCharOneOf('eE'),
        expectMaybeExpector(
            expectCharOneOf('+-')
        ),
        expectDecimalIntegerLiteral
    ])(context);
}

function expectExpectorsAnyCount(expector: TokenExpector): TokenExpector {
    return context => {
        let copyContext = context.getCopy();
        let newContext: TokenExpectorContext | undefined;
        let currentContext = copyContext;
        let resultContext = new TokenExpectorContext(copyContext, new Token('', TokenType.None));
        while (newContext = expector(resultContext.tokenizerContext)) {
            resultContext.token.content += newContext.token.content;
            resultContext.tokenizerContext = newContext.tokenizerContext;
        }
        return resultContext;
    };
}

function expectExpectorsNonZeroCount(expector: TokenExpector): TokenExpector {
    return context => {
        let newContext = expectExpectorsAnyCount(expector)(context);
        if (newContext && newContext.token.content.length != 0)
            return newContext;
        return undefined;
    };
}

function expectExcept(expector: TokenExpector): TokenExpector {
    return context => {
        if (expector(context))
            return undefined;
        let newContext = context.getCopy();
        let symbol = newContext.iterator.getSymbol();
        if (!symbol)
            return undefined;
        return new TokenExpectorContext(newContext, new Token(symbol, TokenType.None));
    }
}

function expectForward(expector: TokenExpector): TokenExpector {
    return context => {
        let copyContext = context.getCopy();
        let newContext = expector(context);
        if (newContext)
            return new TokenExpectorContext(copyContext, new Token('', TokenType.None));
        return undefined;
    }
}

const expectEOF: TokenExpector = context => {
    let copyContext = context.getCopy();
    let newContext = expectUnknownToken(context);
    if (!newContext)
        return new TokenExpectorContext(copyContext, new Token('', TokenType.None));
    else
        return undefined;
}

const expectDecimalIntegerLiteral: TokenExpector =
    expectExpectorsOneOf([
        expectExpectors([
            expectCharOneOf("123456789"),
            expectExpectorsAnyCount(
                expectCharOneOf(digits)
            )
        ]),
        expectChars("0")
    ]);

const expectDecimalDigits: TokenExpector =
    expectExpectorsNonZeroCount(
        expectCharOneOf(digits)
    );

export const expectNumericLiteral: TokenExpector = context => {
    let newContext =
        expectExpectorsOneOf([
            expectExpectors([
                expectDecimalIntegerLiteral,
                expectChars('.'),
                expectMaybeExpector(expectDecimalDigits),
                expectMaybeExpector(expectExponentPart)
            ]),
            expectExpectors([
                expectChars('.'),
                expectDecimalDigits,
                expectMaybeExpector(expectExponentPart)
            ]),
            expectExpectors([
                expectDecimalIntegerLiteral,
                expectMaybeExpector(expectExponentPart)
            ]),
        ])(context);
    if (newContext) {
        newContext.token.type = TokenType.NumericLiteral;
        return newContext;
    }
    return undefined;
};

export const expectWhitespaces: TokenExpector = context => {
    let newContext = expectExpectorsNonZeroCount(expectChars(" "))(context);
    if (newContext) {
        newContext.token.type = TokenType.Whitespaces;
        return newContext;
    } else
        return undefined
}

export const expectLineTerminatorSequence: TokenExpector = context => {
    let newContext = expectExpectorsOneOf([
        expectChars('\r\n'),
        expectChars('\r'),
        expectChars('\n'),
    ])(context);
    if (newContext) {
        newContext.token.type = TokenType.LineTerminatorSequence;
        return newContext;
    }
    return undefined;
}

export const expectComment: TokenExpector = context => {
    let newContext = expectExpectorsOneOf([
        expectExpectors([
            expectChars("//"),
            expectExpectorsAnyCount(
                expectExcept(
                    expectExpectorsOneOf([
                        expectLineTerminatorSequence,
                        expectEOF,
                    ])
                )
            ),
            expectForward(
                expectExpectorsOneOf([
                    expectLineTerminatorSequence,
                    expectEOF
                ])
            )
        ]),
        expectExpectors([
            expectChars("/*"),
            expectExpectorsAnyCount(
                expectExcept(expectChars("*/"))
            ),
            expectChars("*/")
        ]),
    ])(context);
    if (newContext) {
        newContext.token.type = TokenType.Comment;
        return newContext;
    }
    return undefined;
}

export const expectPunctuator: TokenExpector = context => {
    let newContext = expectExpectorsOneOf([
        expectChars("{"), expectChars("}"),
        expectChars("("), expectChars(")"),
        expectChars("["), expectChars("]"),
        expectChars("..."),
        expectChars("."),
        expectChars(";"),
        expectChars(","),
        expectChars("==="), expectChars("!=="),
        expectChars("=="), expectChars("!="),
        expectChars("<="), expectChars(">="),
        expectChars("=>"),
        expectChars("++"), expectChars("--"),
        expectChars("+="), expectChars("-="), expectChars("*="), expectChars("/="), expectChars("%="),
        expectChars("<<="), expectChars(">>="), expectChars(">>>="),
        expectChars("&="), expectChars("|="), expectChars("^="),
        expectChars("**="),
        expectChars("**"),
        expectChars("="),
        expectChars("+"), expectChars("-"), expectChars("*"), expectChars("/"), expectChars("%"),
        expectChars("<<"), expectChars(">>"), expectChars(">>>"),
        expectChars("<"), expectChars(">"),
        expectChars("&&"), expectChars("||"), expectChars("!"),
        expectChars("&"), expectChars("|"), expectChars("^"),
        expectChars("?"), expectChars(":"),
        expectChars("~"),
    ])(context);
    if (newContext) {
        newContext.token.type = TokenType.Punctuator;
        return newContext;
    }
    return undefined;
}
