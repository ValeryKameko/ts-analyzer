import { ProgramSource, ProgramSourceIterator } from "../Program";
import { Token } from ".";
import * as Tokenizers from ".";

export class TokenizerContext {
    constructor(
        public iterator: ProgramSourceIterator) {
    }
    public getCopy(): TokenizerContext {
        return new TokenizerContext(new ProgramSourceIterator(this.iterator));
    }
};


export class Tokenizer {
    constructor(private _source: ProgramSource) {
    }

    public tokenize(): Token[] {
        let tokens: Token[] = [];
        let context = new TokenizerContext(new ProgramSourceIterator(this._source));
        while (true) {
            let newContext: Tokenizers.TokenExpectorContext | undefined = undefined;

            for (var expector of [
                Tokenizers.expectKeyword,
                Tokenizers.expectIdentifier,
                Tokenizers.expectStringLiteral,
                Tokenizers.expectNumericLiteral,
                Tokenizers.expectComment,
                Tokenizers.expectPunctuator,
                Tokenizers.expectWhitespaces,
                Tokenizers.expectLineTerminatorSequence,
                Tokenizers.expectUnknownToken,
            ]) {
                if (newContext = expector(context)) {
                    tokens.push(newContext.token);
                    context = newContext.tokenizerContext;
                    break;    
                }
            }
            if (!newContext)
                break;
        }
        return tokens;
    }
}