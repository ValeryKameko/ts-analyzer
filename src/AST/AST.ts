import { Token, TokenType } from "../Tokenizer/index";
import {
    ASTType
} from "./index";

type Maybe<T> = T | undefined;

export enum ASTNodeType {
    Unknown,
    Token,

    Statement,
    Expression,

    Keyword,
    Identifier,
    Literal,
    Program,
    Type,

    VariableDeclaration,
    ParameterDeclaration,
    FunctionCallParameters,
    ImportClause,
    ObjectProperty
}

export class ASTNode {
    protected _type: ASTNodeType;
    public childs: ASTNode[];
    constructor(
        childs: ASTNode[] = [],
        public parent?: ASTNode) {
        this.childs = childs;
        for (const child of childs) {
            child.parent = this;
        }
        this._type = ASTNodeType.Unknown;
    }
    get type() {
        return this._type;
    }
    protected addChilds(node: Maybe<ASTNode>[]) {
        node.forEach(element => {
            if (element) {
                element.parent = this;
                this.childs.push(element);
            }
        });
    }
}

export class ASTProgram extends ASTNode {
    constructor(
        public statements: ASTNode[] = [],
        parent?: ASTNode) {
        super(statements, parent);
        this._type = ASTNodeType.Program;
    }
}

export class ASTKeyword extends ASTNode {
    constructor(
        public keyword: string = "",
        parent?: ASTNode) {
        super([], parent);
        this._type = ASTNodeType.Keyword;
    }
}

export class ASTIdentifier extends ASTNode {
    constructor(
        public identifier: string = "",
        parent?: ASTNode) {
        super([], parent);
        this._type = ASTNodeType.Identifier;
    }
}

export class ASTFunctionCallParameters extends ASTNode {
    constructor(
        public parameters: ASTExpression[],
        parent?: ASTNode
    ) {
        super(parameters, parent);
        this._type = ASTNodeType.FunctionCallParameters;
    }
}

export enum ASTExpressionType {
    None,
    BracketsEnclosed,

    Literal,
    Identifier,
    FunctionCall,
    IndexAccess,
    MemberAccess,

    PrefixIncrement,
    PrefixDecrement,
    PostfixIncrement,
    PostfixDecrement,

    UnaryMinus,
    UnaryPlus,

    Sum,
    Subtract,
    Multiply,
    Division,
    Modulo,

    Assignment,
    SumAssignment,
    SubtractAssignment,
    MultiplyAssignment,
    DivisionAssignment,
    ModuloAssignment,

    LogicalAnd,
    LogicalOr,
    LogicalNot,

    BitwiseAnd,
    BitwiseOr,
    BitwiseXor,
    BitwiseAndAssignment,
    BitwiseOrAssignment,
    BitwiseXorAssignment,
    BitwiseNot,

    LogicalShiftLeft,
    LogicalShiftRight,
    SignedShiftRight,

    LogicalShiftLeftAssignment,
    LogicalShiftRightAssignment,
    SignedShiftRightAssignment,

    Equal,
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual,

    StrictEqual,
    NotEqual,
    StrictNotEqual,

    Power,
    PowerAssignment,

    TernaryOperator,

    YieldOperator,
    NewWithArgumentsOperator,
    NewWithNoArgumentsOperator,
    SpreadOperator,

    InstanceOfOperator,
    InOperator,
    TypeofOperator,
    VoidOperator,
    DeleteOperator,
}

export class ASTExpression extends ASTNode {
    constructor(
        public operands: ASTNode[] = [],
        public expressionType: ASTExpressionType = ASTExpressionType.None,
        parent?: ASTNode) {
        super([...operands], parent);
        this._type = ASTNodeType.Expression;
    }
}

export class ASTToken extends ASTNode {
    constructor(
        public value: Token = new Token("", TokenType.None),
        parent?: ASTNode) {
        super([], parent);
        this._type = ASTNodeType.Token;
    }
}

export enum ASTStatementType {
    Unknown,
    None,

    Empty,
    VariableDeclaration,
    Expression,
    Block,
    If,
    While,
    DoWhile,
    For,
    Return,
    FunctionDeclaration,
    Continue,
    Break,
    Switch,

    CaseClause,
    DefaultClause,
    ImportStatement
};


export class ASTStatement extends ASTNode {
    public statementType: ASTStatementType = ASTStatementType.None;
    constructor(
        parent?: ASTNode) {
        super([], parent);
        this._type = ASTNodeType.Statement;
    }
}

export class ASTEmptyStatement extends ASTStatement {
    constructor(
        parent?: ASTNode
    ) {
        super(parent);
        this.statementType = ASTStatementType.Empty;
    }
}

export class ASTVariableDeclaration extends ASTNode {
    constructor(
        public identifier = new ASTIdentifier(),
        public initializer: Maybe<ASTExpression> = undefined,
        public varType: Maybe<ASTType> = undefined,
        parent?: ASTNode
    ) {
        super([identifier], parent);
        if (initializer)
            this.addChilds([initializer]);
        if (varType)
            this.addChilds([varType]);
        this._type = ASTNodeType.VariableDeclaration;
    }
}

export class ASTVariableDeclarationStatement extends ASTStatement {
    constructor(
        public declarator: ASTKeyword,
        public variableDeclarations: ASTVariableDeclaration[] = [],
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds(variableDeclarations)
        this.statementType = ASTStatementType.VariableDeclaration;
    }
}

export class ASTExpressionStatement extends ASTStatement {
    constructor(
        public expression: ASTExpression,
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds([expression])
        this.statementType = ASTStatementType.Expression;
    }
}

export class ASTBlockStatement extends ASTStatement {
    constructor(
        public statements: ASTStatement[],
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds(statements)
        this.statementType = ASTStatementType.Block;
    }
}

export class ASTIfStatement extends ASTStatement {
    constructor(
        public condition: ASTExpression,
        public ifStatements: ASTStatement[],
        public elseStatements: ASTStatement[],
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds([condition, ... ifStatements]);
        if (elseStatements)
            this.addChilds([...elseStatements]);
        this.statementType = ASTStatementType.If;
    }
}

export class ASTWhileStatement extends ASTStatement {
    constructor(
        public condition: ASTExpression,
        public body: ASTStatement,
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds([condition, body]);
        this.statementType = ASTStatementType.While;
    }
}

export class ASTDoWhileStatement extends ASTStatement {
    constructor(
        public body: ASTStatement,
        public condition: ASTExpression,
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds([condition, body]);
        this.statementType = ASTStatementType.DoWhile;
    }
}

export class ASTReturnStatement extends ASTStatement {
    constructor(
        public expression: ASTExpression,
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds([expression]);
        this.statementType = ASTStatementType.Return;
    }
}

export class ASTForStatement extends ASTStatement {
    constructor(
        public body: ASTStatement,
        public initializer?: ASTStatement,
        public condition?: ASTExpression,
        public afterExpression?: ASTExpression,
        parent?: ASTNode
    ) {
        super(parent);
        if (initializer)
            this.addChilds([initializer]);
        if (condition)
            this.addChilds([condition]);
        if (afterExpression)
            this.addChilds([afterExpression]);
        this.statementType = ASTStatementType.For;
    }
}

export class ASTParameterDeclaration extends ASTNode {
    constructor(
        public identifier: ASTIdentifier,
        public initializer: Maybe<ASTExpression> = undefined,
        public varType: Maybe<ASTType> = undefined,
        parent?: ASTNode
    ) {
        super([identifier], parent);
        if (initializer)
            this.addChilds([initializer]);
        if (varType)
            this.addChilds([varType]);
        this._type = ASTNodeType.ParameterDeclaration;
    }
}

export class ASTFunctionDeclarationStatement extends ASTStatement {
    constructor(
        public identifier: ASTIdentifier,
        public parameters: ASTParameterDeclaration[],
        public body: ASTStatement,
        public returnType: Maybe<ASTType> = undefined,
        parent?: ASTNode
    ) {
        super(parent);
        if (returnType)
            this.addChilds([returnType]);
        this.addChilds(parameters);
        this.addChilds([identifier, body]);
        this.statementType = ASTStatementType.FunctionDeclaration;
    }
}

export class ASTContinueStatement extends ASTStatement {
    constructor(
        parent?: ASTNode
    ) {
        super(parent);
        this.statementType = ASTStatementType.Continue;
    }
}

export class ASTBreakStatement extends ASTStatement {
    constructor(
        parent?: ASTNode
    ) {
        super(parent);
        this.statementType = ASTStatementType.Break;
    }
}

export class ASTSwitchStatement extends ASTStatement {
    constructor(
        public condition: ASTExpression,
        public clauses: ASTStatement[],
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds([condition]);
        this.addChilds(clauses);
        this.statementType = ASTStatementType.Switch;
    }
}

export class ASTCaseClause extends ASTStatement {
    constructor(
        public expression: ASTExpression,
        public statements: ASTStatement[],
        parent?: ASTNode
    ) {
        super(parent);
        this.addChilds([expression, ...statements]);
        this.statementType = ASTStatementType.CaseClause;
    }
}

export class ASTDefaultClause extends ASTStatement {
    constructor(
        public statements: ASTStatement[],
        parent?: ASTNode
    ) {
        super(parent);
        this.statementType = ASTStatementType.DefaultClause;
    }
}

export class ASTImportClause extends ASTNode {
    constructor(
        public item: ASTNode,
        public asName?: ASTIdentifier,
        parent?: ASTNode
    ) {
        super([item], parent);
        this.addChilds([asName]);
        this._type = ASTNodeType.ImportClause;
    }
}

export class ASTImportStatement extends ASTStatement {
    constructor(
        public importClauses: ASTImportClause[],
        public moduleName: string,
        parent?: ASTNode    
    ) {
        super(parent);
        this.addChilds(importClauses);
        this.statementType = ASTStatementType.ImportStatement;
    }
}