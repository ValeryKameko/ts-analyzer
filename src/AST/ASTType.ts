import {
    ASTNode,
    ASTNodeType,
    ASTIdentifier,
    ASTKeyword
} from "./index";

export class ASTType extends ASTNode {
    constructor(
        public parent?: ASTNode) {
        super([], parent);
        this._type = ASTNodeType.Type;
    }
}

export class ASTUnionType extends ASTType {
    constructor(
        public nodes: ASTType[],
        public parent?: ASTNode) {
        super(parent);
        this.addChilds(nodes);
    }
}

export class ASTIntersectionType extends ASTType {
    constructor(
        public nodes: ASTType[],
        public parent?: ASTNode) {
        super(parent);
        this.addChilds(nodes);
    }
}

export class ASTArrayType extends ASTType {
    constructor(
        public node: ASTType,
        public parent?: ASTNode) {
        super(parent);
        this.addChilds([node]);
    }
}

export class ASTPrimitiveType extends ASTType {
    constructor(
        public keyword: ASTKeyword,
        public parent?: ASTNode) {
        super(parent);
        this.addChilds([keyword]);
    }
}

export class ASTObjectType extends ASTType {
    constructor(
        public keyValues: {
            key: ASTIdentifier,
            value: ASTType
        }[],
        public parent?: ASTNode) {
        super(parent);
        keyValues.forEach(element => {
            this.addChilds([element.key]);
            this.addChilds([element.value]);
        });
    }
}

export class ASTNamedType extends ASTType {
    constructor(
        public name: ASTType,
        public parent?: ASTNode) {
        super(parent);
        this.addChilds([name]);
    }
}