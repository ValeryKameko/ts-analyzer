import {
    ASTNode,
    ASTNodeType,
    ASTKeyword,
    ASTType,
} from "./index";
import { ASTExpression } from "./AST";

export enum ASTLiteralType {
    None,
    Array,
    Object,
    Number,
    String,
    Primitive,
    Function,
}

export class ASTLiteral extends ASTNode {
    public literalType: ASTLiteralType = ASTLiteralType.None;
    constructor(
        public parent?: ASTNode) {
        super([], parent);
        this._type = ASTNodeType.Literal;
    }
}

export class ASTNumberLiteral extends ASTLiteral {
    constructor(
        public value: string,
        public parent?: ASTNode) {
        super(parent);
        this.literalType = ASTLiteralType.Number;
    }
}

export class ASTArrayLiteral extends ASTLiteral {
    constructor(
        public expressions: ASTNode[],
        public parent?: ASTNode) {
        super(parent);
        this.addChilds(expressions);
        this.literalType = ASTLiteralType.Array;
    }
}

export class ASTObjectProperty extends ASTNode {
    constructor(
        public reference: ASTNode,
        public initializer?: ASTExpression,
        public parent?: ASTNode) {
        super([reference], parent);
        this.addChilds([initializer]);
        this._type = ASTNodeType.ObjectProperty;
    }
}

export class ASTObjectLiteral extends ASTLiteral {
    constructor(
        public properties: ASTObjectProperty[],
        parent?: ASTNode) {
        super(parent);
        this.addChilds(properties);
        this.literalType = ASTLiteralType.Object;
    }
}

export class ASTStringLiteral extends ASTLiteral {
    public literalType: ASTLiteralType = ASTLiteralType.String;
    constructor(
        public value: string,
        public parent?: ASTNode) {
        super(parent);
        this.literalType = ASTLiteralType.String;
    }
}

export class ASTPrimitiveLiteral extends ASTLiteral {
    constructor(
        public keyword: ASTKeyword,
        public parent?: ASTNode) {
        super(parent);
        this.addChilds([keyword]);
        this.literalType = ASTLiteralType.Primitive;
    }
}

export class ASTFunctionLiteral extends ASTLiteral {
    constructor(
        public parameters: ASTNode[], 
        public body: ASTNode,
        public returnType?: ASTType,
        public parent?: ASTNode) {
        super(parent);
        this.addChilds(parameters);
        this.addChilds([body, returnType]);
        this.literalType = ASTLiteralType.Function;
    }
}
