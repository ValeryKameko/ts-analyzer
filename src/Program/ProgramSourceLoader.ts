import { readFileSync } from "fs";
import { ProgramSource } from ".";

export class ProgramSourceLoader {
    private _source: ProgramSource;
    private _filePath: string;
    constructor(filePath: string) {
        this._filePath = filePath;
        this._source = new ProgramSource("");
    }
    public load(): void {
        let fileContent = readFileSync(this._filePath, { encoding: 'utf-8', flag: 'r' });
        this._source.text = fileContent;
    }
    get source() {
        return this._source;
    }
}