export class ProgramSource {
    public text: string;
    constructor(text: string = "") {
        this.text = text;
    }
}

export class ProgramSourceIterator {
    private _textPosition: number;
    private _source: ProgramSource;

    constructor(source: ProgramSource | ProgramSourceIterator) {
        if (source instanceof ProgramSourceIterator) {
            this._textPosition = source._textPosition;
            this._source = source._source;
            return
        }
        this._textPosition = 0;
        this._source = source;
    }
    public get textPosition() {
        return this._textPosition;
    }
    public set textPosition(position: number) {
        this._textPosition = position;
    }
    public getSymbols(count: number): string | undefined {
        let symbols = this._source.text.substr(this._textPosition, count);
        if (symbols.length !== count)
            return undefined;
        this._textPosition += count;
        return symbols;
    }
    public getSymbol(): string | undefined {
        return this.getSymbols(1);
    }
    public get source() {
        return this._source;
    }
}