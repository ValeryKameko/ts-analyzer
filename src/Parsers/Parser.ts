import * as ParserExpectors from "./ParserExpectors";
import { Token, TokenType } from "../Tokenizer/index";
import * as AST from "../AST/index";
import { expectExpectorsOneOf, expectExpectorAnyCount } from "./AbstractExpector";

type Maybe<T> = T | undefined;

export class Parser {
    constructor(private _stream: Token[]) {
    }

    parse(): Maybe<AST.ASTNode> {
        let nodeStream: AST.ASTNode[] = this._stream.filter(token => {
            return ![TokenType.Whitespaces, TokenType.Comment, TokenType.LineTerminatorSequence, TokenType.Unknown].includes(token.type);
        }).map(token => {
            let astNode = new AST.ASTToken();
            astNode.value = token;
            return astNode;
        });

        let currentContext: Maybe<ParserExpectors.ASTExpectorReturnContext> = {
            context: new ParserExpectors.ASTExpectorContext(nodeStream),
            elements: []
        }

        currentContext = ParserExpectors.expectASTProgram(currentContext.context);

        if (currentContext)
            return currentContext.elements[0];
        return undefined;
    }
}