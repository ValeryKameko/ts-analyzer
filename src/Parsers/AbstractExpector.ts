type Maybe<T> = T | undefined;

export class AbstractExpectorContext<Element> {
    private _index: number;
    constructor(private _stream: Element[]) {
        this._index = 0;
    }
    getElements(count: number): Maybe<Element[]> {
        let result: Element[] = [];
        if (this._index + count > this._stream.length)
            return undefined;
        while (this._index < this._stream.length && result.length < count) {
            result.push(this._stream[this._index])
            this._index++;
        }
        return result;
    }
    getElement(): Maybe<Element> {
        let elements = this.getElements(1);
        if (elements)
            return elements[0]
        return undefined;
    }
    getCopy() {
        let copy = new AbstractExpectorContext<Element>(this._stream);
        copy._index = this._index;
        return copy;
    }
}

export type AbstractExpectorReturnContext<Element> = {
    elements: Element[];
    context: AbstractExpectorContext<Element>;
};

export type AbstractExpector<Element> =
    (context: AbstractExpectorContext<Element>) => Maybe<AbstractExpectorReturnContext<Element>>;

export type AbstractExpectorFilter<Element> =
    (a: Element) => boolean;

export type AbstractExpectorComparator<Element> =
    (a: Element, b: Element) => boolean;

export function expectFilter<Element>(
    filter: AbstractExpectorFilter<Element>
): AbstractExpector<Element> {
    return context => {
        let newContext = context.getCopy();
        let element = newContext.getElement();
        if (!element || !filter(element))
            return undefined;
        return {
            elements: [element],
            context: newContext,
        };
    };
}

export function expectExpectors<Element>(
    expectors: AbstractExpector<Element>[]
): AbstractExpector<Element> {
    return context => {
        let returnContext: AbstractExpectorReturnContext<Element> = {
            elements: [],
            context: context.getCopy()
        };
        for (const expector of expectors) {
            let currentReturnContext = expector(returnContext.context);
            if (!currentReturnContext)
                return undefined;
            returnContext.context = currentReturnContext.context;
            returnContext.elements.push(...currentReturnContext.elements);
        }
        return returnContext;
    }
}

export function expectExpectorsOneOf<Element>(
    expectors: AbstractExpector<Element>[]
): AbstractExpector<Element> {
    return context => {
        for (const expector of expectors) {
            let returnContext = expector(context);
            if (returnContext)
                return returnContext;
        }
        return undefined;
    }
}

export function expectExpectorAnyCount<Element>(
    expector: AbstractExpector<Element>
): AbstractExpector<Element> {
    return context => {
        let returnContext = expectExpectorNonZeroCount(expector)(context);
        if (!returnContext)
            return {
                elements: [],
                context: context
            };
        return returnContext;
    };
}

export function expectExpectorNonZeroCount<Element>(
    expector: AbstractExpector<Element>
): AbstractExpector<Element> {
    return context => {
        let returnContext: AbstractExpectorReturnContext<Element> = {
            elements: [],
            context: context.getCopy()
        };
        let count = 0;
        let currentReturnContext = undefined;
        while (currentReturnContext = expector(returnContext.context)) {
            returnContext.elements.push(...currentReturnContext.elements);
            returnContext.context = currentReturnContext.context;
            count++;
        }
        if (count == 0)
            return undefined;
        return returnContext;
    };
}

export function expectMaybeExpector<Element>(
    expector: AbstractExpector<Element>
): AbstractExpector<Element> {
    return context => {
        let returnContext = expector(context);
        if (returnContext)
            return returnContext;
        return {
            elements: [],
            context: context
        }
    };
}

export function expectForwardExpector<Element>(
    expector: AbstractExpector<Element>
): AbstractExpector<Element> {
    return context => {
        let returnContext = expector(context);
        if (returnContext)
            return {
                elements: [],
                context: context
            };
        return undefined;
    }
}

export function expectExceptExpector<Element>(
    expector: AbstractExpector<Element>
): AbstractExpector<Element> {
    return context => {
        let returnContext = expectForwardExpector(expector)(context);
        if (returnContext)
            return undefined;
        return {
            elements: [],
            context: context
        };
    }
}

export function expectAnyForward<Element>(): AbstractExpector<Element> {
    return context => {
        return expectForwardExpector(
            expectFilter((x: Element) => true)
        )(context);
    }
}
