import * as AST from "../AST/index";
import { Token, TokenType, expectKeyword } from "../Tokenizer/index";
import {
    ASTExpressionType,
    ASTExpression,
    ASTToken,
    ASTStringLiteral,
    ASTNumberLiteral,
    ASTNode,
    ASTArrayLiteral,
    ASTVariableDeclaration,
    ASTType,
    ASTIdentifier,
    ASTVariableDeclarationStatement,
    ASTExpressionStatement,
    ASTStatement,
    ASTBlockStatement,
    ASTIfStatement,
    ASTWhileStatement,
    ASTReturnStatement,
    ASTForStatement,
    ASTParameterDeclaration,
    ASTFunctionDeclarationStatement,
    ASTContinueStatement,
    ASTBreakStatement,
    ASTCaseClause,
    ASTDefaultClause,
    ASTSwitchStatement,
    ASTFunctionCallParameters,
    ASTArrayType,
    ASTPrimitiveLiteral,
    ASTKeyword,
    ASTEmptyStatement,
    ASTProgram,
    ASTPrimitiveType,
    ASTDoWhileStatement,
    ASTStatementType,
    ASTImportClause,
    ASTImportStatement,
    ASTObjectLiteral,
    ASTObjectProperty,
    ASTObjectType,
    ASTLiteral,
} from "../AST/index";
import { ASTNamedType } from "../AST/index";
import {
    AbstractExpectorContext,
    AbstractExpectorReturnContext,
    AbstractExpectorFilter,
    AbstractExpectorComparator,
    expectExpectorsOneOf,
    expectExpectors,
    expectExpectorAnyCount,
    expectMaybeExpector,
    expectFilter,
    AbstractExpector,
    expectExpectorNonZeroCount,
    expectAnyForward,

} from "./AbstractExpector";
import { checkServerIdentity } from "tls";
import { format } from "url";
import { maybeModifyStringNFA } from "../Automata";


type Maybe<T> = T | undefined;

export enum Associativity {
    LeftAssociative,
    RightAssociative
}

export type ASTExpectorReturnContext =
    AbstractExpectorReturnContext<AST.ASTNode>;

export type ASTExpector =
    AbstractExpector<AST.ASTNode>;

export type ASTExpectorFilter =
    AbstractExpectorFilter<AST.ASTNode>;

export type ASTExpectorComparator =
    AbstractExpectorComparator<AST.ASTNode>;

// Tokens

export function expectASTToken(token: Token): ASTExpector {
    return expectFilter(node => {
        if (node.type == AST.ASTNodeType.Token) {
            let otherToken = (<AST.ASTToken>node).value;
            return ((otherToken.type == token.type) || (token.type == TokenType.None)) &&
                ((otherToken.content == token.content) || (token.content == ""));
        }
        return false;
    });
}

export function expectASTPunctuatorToken(punctuator: string): ASTExpector {
    return expectASTToken(new Token(punctuator, TokenType.Punctuator));
}

export class ASTExpectorContext extends AbstractExpectorContext<AST.ASTNode>{

}

export function expectASTKeywordToken(keyword: string): ASTExpector {
    return context => {
        let returnContext = expectASTToken(new Token(keyword, TokenType.Keyword))(context);
        if (returnContext) {
            returnContext.elements = returnContext.elements.map(node => {
                let keywordNode = new AST.ASTKeyword();
                keywordNode.keyword = (<AST.ASTToken>node).value.content;
                return keywordNode;
            });
            return returnContext;
        }
        return undefined;
    }
}

export function expectASTIdentifier(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTToken(
        new Token("", TokenType.Identifier)
    )(context);
    if (!returnContext)
        return undefined;
    returnContext.elements = returnContext.elements.map(node => {
        let identifierNode = new AST.ASTIdentifier();
        identifierNode.identifier = (<AST.ASTToken>node).value.content;
        return identifierNode;
    });
    return returnContext;
}

// Expressions

export function expectASTIdentifierExpression(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTIdentifier(context);
    if (!returnContext)
        return undefined;
    let node = new ASTExpression([returnContext.elements[0]], ASTExpressionType.Identifier);
    returnContext.elements = [node];
    return returnContext;
}

function classifyBinaryOperator(
    token: ASTToken
): ASTExpressionType {
    if (token.value.content == "+")
        return ASTExpressionType.Sum;
    if (token.value.content == "-")
        return ASTExpressionType.Subtract;
    if (token.value.content == "*")
        return ASTExpressionType.Multiply;
    if (token.value.content == "/")
        return ASTExpressionType.Division;
    if (token.value.content == "%")
        return ASTExpressionType.Modulo;
    if (token.value.content == "=")
        return ASTExpressionType.Assignment;
    if (token.value.content == "+=")
        return ASTExpressionType.SumAssignment;
    if (token.value.content == "-=")
        return ASTExpressionType.SubtractAssignment;
    if (token.value.content == "*=")
        return ASTExpressionType.MultiplyAssignment;
    if (token.value.content == "/=")
        return ASTExpressionType.ModuloAssignment;
    if (token.value.content == "&&")
        return ASTExpressionType.LogicalAnd;
    if (token.value.content == "||")
        return ASTExpressionType.LogicalOr;
    if (token.value.content == "&")
        return ASTExpressionType.BitwiseAnd;
    if (token.value.content == "|")
        return ASTExpressionType.BitwiseOr;
    if (token.value.content == "^")
        return ASTExpressionType.BitwiseXor;
    if (token.value.content == "&=")
        return ASTExpressionType.BitwiseAndAssignment;
    if (token.value.content == "|=")
        return ASTExpressionType.BitwiseOrAssignment;
    if (token.value.content == "^=")
        return ASTExpressionType.BitwiseXorAssignment;
    if (token.value.content == "<<")
        return ASTExpressionType.LogicalShiftLeft;
    if (token.value.content == "<<")
        return ASTExpressionType.LogicalShiftLeft;
    if (token.value.content == ">>")
        return ASTExpressionType.LogicalShiftRight;
    if (token.value.content == ">>>")
        return ASTExpressionType.SignedShiftRightAssignment;
    if (token.value.content == "==")
        return ASTExpressionType.Equal;
    if (token.value.content == "<")
        return ASTExpressionType.Less;
    if (token.value.content == "<=")
        return ASTExpressionType.LessOrEqual;
    if (token.value.content == ">")
        return ASTExpressionType.Greater;
    if (token.value.content == ">=")
        return ASTExpressionType.GreaterOrEqual;
    if (token.value.content == "===")
        return ASTExpressionType.StrictEqual;
    if (token.value.content == "!=")
        return ASTExpressionType.NotEqual;
    if (token.value.content == "!==")
        return ASTExpressionType.StrictNotEqual;
    if (token.value.content == "in")
        return ASTExpressionType.InOperator;
    if (token.value.content == "instanceof")
        return ASTExpressionType.InstanceOfOperator;
    return ASTExpressionType.None;
}

function classifyPrefixUnaryOperator(
    token: ASTToken
): ASTExpressionType {
    if (token.value.content == "++")
        return ASTExpressionType.PrefixIncrement;
    if (token.value.content == "--")
        return ASTExpressionType.PrefixDecrement;
    if (token.value.content == "+")
        return ASTExpressionType.UnaryPlus;
    if (token.value.content == "-")
        return ASTExpressionType.UnaryMinus;
    if (token.value.content == "...")
        return ASTExpressionType.SpreadOperator;
    if (token.value.content == "yield")
        return ASTExpressionType.YieldOperator;
    if (token.value.content == "new")
        return ASTExpressionType.NewWithNoArgumentsOperator;
    if (token.value.content == "typeof")
        return ASTExpressionType.TypeofOperator;
    if (token.value.content == "void")
        return ASTExpressionType.VoidOperator;
    if (token.value.content == "delete")
        return ASTExpressionType.DeleteOperator;
    return ASTExpressionType.None;
}

function classifyPostfixUnaryOperator(
    token: ASTToken
): ASTExpressionType {
    if (token.value.content == "++")
        return ASTExpressionType.PostfixIncrement;
    if (token.value.content == "--")
        return ASTExpressionType.PostfixDecrement;
    return ASTExpressionType.None;
}

export function expectASTBinaryExpression(
    elementExpector: ASTExpector,
    operatorExpector: ASTExpector,
    associativity: Associativity
): ASTExpector {
    return context => {
        let returnContext = expectExpectors([
            elementExpector,
            expectExpectorAnyCount(
                expectExpectors([
                    operatorExpector,
                    elementExpector
                ])
            )
        ])(context);
        if (!returnContext)
            return undefined;
        let node;
        if (associativity == Associativity.LeftAssociative) {
            node = returnContext.elements[0];
            for (let i = 1; i < returnContext.elements.length; i += 2) {
                let operator = <ASTToken>(returnContext.elements[i]);
                node = new ASTExpression(
                    [node, returnContext.elements[i + 1]],
                    classifyBinaryOperator(operator)
                );
            }
        } else {
            node = returnContext.elements[returnContext.elements.length - 1];
            for (let i = returnContext.elements.length - 2; i >= 1; i -= 2) {
                let operator = <ASTToken>(returnContext.elements[i]);
                node = new ASTExpression(
                    [returnContext.elements[i - 1], node],
                    classifyBinaryOperator(operator)
                );
            }
        }
        returnContext.elements = [node];
        return returnContext;
    }
}


function expectASTBracketsEnclosedExpression(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTToken(new Token("(", TokenType.Punctuator)),
        expectASTExpression,
        expectASTToken(new Token(")", TokenType.Punctuator))
    ])(context);
    if (!returnContext)
        return undefined;
    let node = new ASTExpression([returnContext.elements[1]], ASTExpressionType.BracketsEnclosed);
    returnContext.elements = [node];
    return returnContext;
}

function expectASTPrefixUnaryExpression(
    operatorExpector: ASTExpector,
    elementExpector: ASTExpector
): ASTExpector {
    return context => {
        let returnContext = expectExpectors([
            expectExpectorAnyCount(
                operatorExpector
            ),
            elementExpector
        ])(context);
        if (!returnContext)
            return undefined;
        let node = returnContext.elements[returnContext.elements.length - 1];
        for (let i = returnContext.elements.length - 2; i >= 0; i--) {
            let operator = <ASTToken>(returnContext.elements[i]);
            node = new ASTExpression([node], classifyPrefixUnaryOperator(operator));
        }
        returnContext.elements = [node];
        return returnContext;
    }
}

function expectASTPostfixUnaryExpression(
    elementExpector: ASTExpector,
    operatorExpector: ASTExpector
): ASTExpector {
    return context => {
        let returnContext = expectExpectors([
            elementExpector,
            expectExpectorAnyCount(
                operatorExpector
            )
        ])(context);
        if (!returnContext)
            return undefined;
        let node = returnContext.elements[0];
        for (let i = 1; i < returnContext.elements.length; i++) {
            let operator = <ASTToken>(returnContext.elements[i]);
            node = new ASTExpression([node], classifyPostfixUnaryOperator(operator));
        }
        returnContext.elements = [node];
        return returnContext;
    }
}

function expectASTFunctionCallParameters(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectMaybeExpector(
        expectExpectors([
            expectASTExpression,
            expectExpectorAnyCount(
                expectExpectors([
                    expectASTPunctuatorToken(","),
                    expectASTExpression
                ])
            )
        ])
    )(context);
    if (!returnContext)
        return undefined;
    let parameters: ASTExpression[] = [];
    for (const e of returnContext.elements) {
        if (e instanceof ASTExpression)
            parameters.push(e);
    }
    let node = new ASTFunctionCallParameters(parameters);
    returnContext.elements = [node];
    return returnContext;
}

function expectASTFunctionCall(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTExpressionLevel18,
        expectExpectorAnyCount(
            expectExpectors([
                expectASTPunctuatorToken("("),
                expectASTFunctionCallParameters,
                expectASTPunctuatorToken(")")
            ])
        )
    ])(context);
    if (!returnContext)
        return undefined;
    let node = returnContext.elements[0];
    for (const e of returnContext.elements) {
        if (e instanceof ASTFunctionCallParameters)
            node = new ASTExpression(
                [node, e],
                ASTExpressionType.FunctionCall
            );
    }
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTExpressionLevel0(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTExpressionLevel1(context);
}

export function expectASTExpressionLevel1(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTPrefixUnaryExpression(
        expectASTPunctuatorToken("..."),
        expectASTExpressionLevel2,
    )(context);
}

export function expectASTExpressionLevel2(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTPrefixUnaryExpression(
        expectASTKeywordToken("yield"),
        expectASTExpressionLevel3
    )(context);
}

export function expectASTExpressionLevel3(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel4,
        expectExpectorsOneOf([
            expectASTPunctuatorToken("="),
            expectASTPunctuatorToken("+="),
            expectASTPunctuatorToken("-="),
            expectASTPunctuatorToken("*="),
            expectASTPunctuatorToken("/="),
            expectASTPunctuatorToken("%="),
            expectASTPunctuatorToken("<<="),
            expectASTPunctuatorToken(">>="),
            expectASTPunctuatorToken(">>>="),
            expectASTPunctuatorToken("&="),
            expectASTPunctuatorToken("^="),
            expectASTPunctuatorToken("|="),
        ]),
        Associativity.RightAssociative
    )(context);
}

export function expectASTExpressionLevel4(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTExpressionLevel5(context);
}

export function expectASTExpressionLevel5(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel6,
        expectASTPunctuatorToken("||"),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel6(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel7,
        expectASTPunctuatorToken("&&"),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel7(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel8,
        expectASTPunctuatorToken("|"),
        Associativity.LeftAssociative
    )(context);

}

export function expectASTExpressionLevel8(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel9,
        expectASTPunctuatorToken("^"),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel9(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel10,
        expectASTPunctuatorToken("&"),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel10(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel11,
        expectExpectorsOneOf([
            expectASTPunctuatorToken("=="),
            expectASTPunctuatorToken("==="),
            expectASTPunctuatorToken("!="),
            expectASTPunctuatorToken("!=="),
        ]),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel11(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel12,
        expectExpectorsOneOf([
            expectASTPunctuatorToken("<="),
            expectASTPunctuatorToken(">="),
            expectASTPunctuatorToken(">"),
            expectASTPunctuatorToken("<"),
            expectASTKeywordToken("in"),
            expectASTKeywordToken("instanceof"),
        ]),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel12(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel13,
        expectExpectorsOneOf([
            expectASTPunctuatorToken(">>"),
            expectASTPunctuatorToken("<<"),
            expectASTPunctuatorToken(">>>"),
        ]),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel13(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel14,
        expectExpectorsOneOf([
            expectASTPunctuatorToken("+"),
            expectASTPunctuatorToken("-"),
        ]),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel14(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTBinaryExpression(
        expectASTExpressionLevel15,
        expectExpectorsOneOf([
            expectASTPunctuatorToken("*"),
            expectASTPunctuatorToken("/"),
            expectASTPunctuatorToken("%"),
        ]),
        Associativity.LeftAssociative
    )(context);
}

export function expectASTExpressionLevel15(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTPrefixUnaryExpression(
        expectExpectorsOneOf([
            expectASTPunctuatorToken("!"),
            expectASTPunctuatorToken("~"),
            expectASTPunctuatorToken("+"),
            expectASTPunctuatorToken("-"),
            expectASTPunctuatorToken("++"),
            expectASTPunctuatorToken("--"),
            expectASTKeywordToken("typeof"),
            expectASTKeywordToken("void"),
            expectASTKeywordToken("delete"),
        ]),
        expectASTExpressionLevel16
    )(context);
}

export function expectASTExpressionLevel16(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTPostfixUnaryExpression(
        expectASTExpressionLevel17,
        expectExpectorsOneOf([
            expectASTPunctuatorToken("++"),
            expectASTPunctuatorToken("--")
        ])
    )(context);
}

export function expectASTExpressionLevel17(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTFunctionCall(context);
}

export function expectASTExpressionLevel18(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTExpressionLevel19,
        expectExpectorAnyCount(
            expectExpectorsOneOf([
                expectExpectors([
                    expectASTPunctuatorToken("."),
                    expectASTExpressionLevel19
                ]),
                expectExpectors([
                    expectASTPunctuatorToken("["),
                    expectASTExpressionLevel19,
                    expectASTPunctuatorToken("]")
                ])
            ])
        )
    ])(context);
    if (!returnContext)
        return undefined;
    let node = returnContext.elements[0];
    for (let i = 1; i < returnContext.elements.length; i++) {
        if (returnContext.elements[i] instanceof ASTExpression) {
            let prevPunctuator = (<ASTToken>returnContext.elements[i - 1]).value.content;
            if (prevPunctuator == ".")
                node = new ASTExpression([node, returnContext.elements[i]], ASTExpressionType.MemberAccess);
            else if (prevPunctuator == "[")
                node = new ASTExpression([node, returnContext.elements[i]], ASTExpressionType.IndexAccess);
        }
    }
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTExpressionLevel19(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectExpectorsOneOf([
        expectASTBracketsEnclosedExpression,
        expectASTLiteralExpression,
        expectASTIdentifierExpression,
    ])(context);
}

export function expectASTExpression(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectASTExpressionLevel0(context);
}

// Literals

export function expectASTNumberLiteral(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTToken(
        new Token("", TokenType.NumericLiteral)
    )(context);
    if (!returnContext)
        return undefined;
    let numberRepresentation = (<ASTToken>returnContext.elements[0]).value.content;
    let node = new ASTNumberLiteral(numberRepresentation);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTStringLiteral(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTToken(
        new Token("", TokenType.StringLiteral)
    )(context);
    if (!returnContext)
        return undefined;
    let stringRepresentation = (<ASTToken>returnContext.elements[0]).value.content;
    let node = new ASTStringLiteral(
        stringRepresentation.slice(1, stringRepresentation.length - 1)
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTPrimitiveLiteral(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectorsOneOf([
        expectASTKeywordToken("undefined"),
        expectASTKeywordToken("null"),
        expectASTKeywordToken("true"),
        expectASTKeywordToken("false")
    ])(context);
    if (!returnContext)
        return undefined;
    let keyword = <ASTKeyword>returnContext.elements[0];
    let node = new ASTPrimitiveLiteral(keyword);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTArrayLiteral(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTPunctuatorToken("["),
        expectMaybeExpector(
            expectExpectors([
                expectASTExpression,
                expectExpectorAnyCount(
                    expectExpectors([
                        expectASTPunctuatorToken(","),
                        expectASTExpression
                    ])
                )
            ])
        ),
        expectASTPunctuatorToken("]")
    ])(context);
    if (!returnContext)
        return undefined;
    let arr: ASTExpression[] = [];
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTExpression)
            arr.push(elem);
    }
    let node = new ASTArrayLiteral(arr);
    returnContext.elements = [node];
    return returnContext;
}

export function expefctASTObjectProperty(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectorsOneOf([
        expectExpectors([
            expectExpectorsOneOf([
                expectASTIdentifier,
                expectASTStringLiteral,
                expectASTNumberLiteral
            ]),
            expectASTPunctuatorToken(":"),
            expectASTExpression
        ]),
        expectASTIdentifier
    ])(context);
    if (!returnContext)
        return undefined;
    
    let reference: ASTNode = new ASTNode();
    let initializer: Maybe<ASTExpression> = undefined;

    for (const node of returnContext.elements) {
        if ((node instanceof ASTIdentifier) ||
            (node instanceof ASTLiteral)
        ) {
            reference = node;
        }
        if (node instanceof ASTExpression) {
            initializer = node;
        }
    }
    let node = new ASTObjectProperty(reference, initializer);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTObjectLiteral(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTPunctuatorToken("{"),
        expectMaybeExpector(
            expectExpectors([
                expefctASTObjectProperty,
                expectExpectorAnyCount(
                    expectExpectors([
                        expectASTPunctuatorToken(","),
                        expefctASTObjectProperty
                    ])
                )
            ])
        ),
        expectASTPunctuatorToken("}"),
    ])(context);
    if (!returnContext)
        return undefined;
    let properties: ASTObjectProperty[] = [];
    for (const node of returnContext.elements) {
        if (node instanceof ASTObjectProperty) {
            properties.push(node);
        }
    }
    let node = new ASTObjectLiteral(properties);
}

export function expectASTLiteral(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    return expectExpectorsOneOf([
        expectASTArrayLiteral,
        expectASTNumberLiteral,
        expectASTStringLiteral,
        expectASTPrimitiveLiteral,
        expectASTObjectLiteral
    ])(context);
}

export function expectASTLiteralExpression(context: ASTExpectorContext): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTLiteral(context);
    if (!returnContext)
        return undefined;
    let node = new ASTExpression([returnContext.elements[0]], ASTExpressionType.Literal);
    returnContext.elements = [node];
    return returnContext;
}

// Types

export function expectASTPrimitiveType(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectorsOneOf([
        expectASTKeywordToken("boolean"),
        expectASTKeywordToken("null"),
        expectASTKeywordToken("undefined"),
        expectASTKeywordToken("string"),
        expectASTKeywordToken("number"),
        expectASTKeywordToken("any")
    ])(context);
    if (!returnContext)
        return undefined;
    let node = new ASTPrimitiveType(
        <ASTKeyword>returnContext.elements[0]
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTNamedType(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTIdentifier(context);
    if (!returnContext)
        return undefined;
    let node = new ASTNamedType(<ASTType>returnContext.elements[0])
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTArrayType(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTTypeLevel1,
        expectExpectorAnyCount(
            expectExpectors([
                expectASTPunctuatorToken("["),
                expectASTPunctuatorToken("]"),
            ])
        )
    ])(context);
    if (!returnContext)
        return undefined;
    let node = <ASTType>returnContext.elements[0];
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTToken && elem.value.content == "]")
            node = new ASTArrayType(node);
    }
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTTypeLevel0(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    return expectExpectorsOneOf([
        expectASTArrayType
    ])(context);
}

export function expectASTTypeLevel1(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    return expectExpectorsOneOf([
        expectASTPrimitiveType,
        expectASTNamedType
    ])(context);
}

export function expectASTType(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    return expectExpectorsOneOf([
        expectASTTypeLevel0
    ])(context);
}

// Statements

export function expectASTVariableDeclaration(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTIdentifier,
        expectMaybeExpector(
            expectExpectors([
                expectASTPunctuatorToken(":"),
                expectASTType
            ])
        ),
        expectMaybeExpector(
            expectExpectors([
                expectASTPunctuatorToken("="),
                expectASTExpression
            ])
        )
    ])(context);
    if (!returnContext)
        return undefined;
    let identifier = <ASTIdentifier>returnContext.elements[0];
    let maybeType: Maybe<ASTType> = undefined;
    let maybeInitializer: Maybe<ASTExpression> = undefined;
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTType)
            maybeType = elem;
        if (elem instanceof ASTExpression)
            maybeInitializer = elem;
    }
    let node = new ASTVariableDeclaration(
        identifier,
        maybeInitializer,
        maybeType
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTVariableDeclarationStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectExpectorsOneOf([
            expectASTKeywordToken("var"),
            expectASTKeywordToken("let"),
            expectASTKeywordToken("const"),
        ]),
        expectASTVariableDeclaration,
        expectExpectorAnyCount(
            expectExpectors([
                expectASTPunctuatorToken(","),
                expectASTVariableDeclaration
            ])
        )
    ])(context);
    if (!returnContext)
        return undefined;
    let declarator = <ASTKeyword>returnContext.elements[0];
    let variableDeclarations: ASTVariableDeclaration[] = [];
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTVariableDeclaration)
            variableDeclarations.push(elem);
    }
    let node = new ASTVariableDeclarationStatement(
        declarator,
        variableDeclarations
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTEmptyStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTPunctuatorToken(";")(context);
    if (!returnContext)
        return undefined;
    let node = new ASTEmptyStatement();
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTExpressionStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTExpression(context);
    if (!returnContext)
        return undefined;
    let node = new ASTExpressionStatement(
        <ASTExpression>returnContext.elements[0]
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTIfStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("if"),
        expectASTPunctuatorToken("("),
        expectASTExpression,
        expectASTPunctuatorToken(")"),
        expectASTStatement,
        expectMaybeExpector(
            expectASTEmptyStatement
        ),
        expectMaybeExpector(
            expectExpectors([
                expectASTKeywordToken("else"),
                expectASTStatement
            ])
        )
    ])(context);
    if (!returnContext)
        return undefined;

    let condition = <ASTExpression>returnContext.elements[2];
    let ifStatements: ASTStatement[] = [];
    let elseStatemens: ASTStatement[] = [];
    let wasElse = false;
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTKeyword && elem.keyword == "else")
            wasElse = true;
        if (elem instanceof ASTStatement && !wasElse)
            ifStatements.push(elem);
        if (elem instanceof ASTStatement && wasElse)
            elseStatemens.push(elem);
    }
    let node = new ASTIfStatement(
        condition,
        ifStatements,
        elseStatemens
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTWhileStatment(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("while"),
        expectASTPunctuatorToken("("),
        expectASTExpression,
        expectASTPunctuatorToken(")"),
        expectASTStatement
    ])(context);
    if (!returnContext)
        return undefined;
    let condition = <ASTExpression>returnContext.elements[2];
    let statement = <ASTStatement>returnContext.elements[4];
    let node = new ASTWhileStatement(
        condition,
        statement
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTForStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("for"),
        expectASTPunctuatorToken("("),
        expectMaybeExpector(
            expectExpectorsOneOf([
                expectASTVariableDeclarationStatement,
                expectASTExpressionStatement
            ])
        ),
        expectASTPunctuatorToken(";"),
        expectMaybeExpector(
            expectExpectorsOneOf([
                expectASTExpression,
            ])
        ),
        expectASTPunctuatorToken(";"),
        expectMaybeExpector(
            expectExpectorsOneOf([
                expectASTExpression,
            ])
        ),
        expectASTPunctuatorToken(")"),
        expectASTStatement
    ])(context);
    if (!returnContext)
        return undefined;

    let initializer: Maybe<ASTStatement> = undefined;
    let condition: Maybe<ASTExpression> = undefined;
    let afterExpression: Maybe<ASTExpression> = undefined;
    let statement: Maybe<ASTStatement> = undefined;
    let semicolonCount = 0;
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTToken &&
            (<ASTToken>elem).value.content == ";")
            semicolonCount++;
        if (elem instanceof ASTStatement &&
            semicolonCount == 0)
            initializer = elem;
        if (elem instanceof ASTExpression &&
            semicolonCount == 1)
            condition = elem;
        if (elem instanceof ASTExpression &&
            semicolonCount == 2)
            afterExpression = elem;
        if (elem instanceof ASTStatement &&
            semicolonCount == 2)
            statement = elem;
    }
    if (!statement)
        return undefined;

    let node = new ASTForStatement(
        statement,
        initializer,
        condition,
        afterExpression
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTDoWhileStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("do"),
        expectASTStatement,
        expectASTKeywordToken("while"),
        expectASTPunctuatorToken("("),
        expectASTExpression,
        expectASTPunctuatorToken(")")
    ])(context);
    if (!returnContext)
        return undefined;
    let statement = <ASTStatement>returnContext.elements[1];
    let condition = <ASTExpression>returnContext.elements[4];
    let node = new ASTDoWhileStatement(statement, condition);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTBlockStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTPunctuatorToken("{"),
        expectExpectorAnyCount(
            expectASTStatement
        ),
        expectASTPunctuatorToken("}")
    ])(context);
    if (!returnContext)
        return undefined;
    let statements: ASTStatement[] = []
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTStatement)
            statements.push(elem);
    }
    let node = new ASTBlockStatement(statements);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTReturnStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("return"),
        expectASTExpression
    ])(context);
    if (!returnContext)
        return undefined;
    let expression = <ASTExpression>returnContext.elements[1];
    let node = new ASTReturnStatement(expression);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTBreakStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTKeywordToken("break")(context);
    if (!returnContext)
        return undefined;
    let node = new ASTBreakStatement();
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTContinueStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectASTKeywordToken("continue")(context);
    if (!returnContext)
        return undefined;
    let node = new ASTContinueStatement();
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTParameterDeclaration(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTIdentifier,
        expectMaybeExpector(
            expectExpectors([
                expectASTPunctuatorToken(":"),
                expectASTType
            ])
        ),
        expectMaybeExpector(
            expectExpectors([
                expectASTPunctuatorToken("="),
                expectASTExpression
            ])
        ),
    ])(context);
    if (!returnContext)
        return undefined;
    let identifier = <ASTIdentifier>returnContext.elements[0];
    let initializer: Maybe<ASTExpression> = undefined;
    let varType: Maybe<ASTType> = undefined;
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTExpression)
            initializer = elem;
        if (elem instanceof ASTType)
            varType = elem;
    }
    let node = new ASTParameterDeclaration(
        identifier,
        initializer,
        varType
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTFunctionDeclaration(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("function"),
        expectASTIdentifier,
        expectASTPunctuatorToken("("),
        expectMaybeExpector(
            expectExpectors([
                expectASTParameterDeclaration,
                expectExpectorAnyCount(
                    expectExpectors([
                        expectASTPunctuatorToken(","),
                        expectASTParameterDeclaration
                    ])
                )
            ])
        ),
        expectASTPunctuatorToken(")"),
        expectMaybeExpector(
            expectExpectors([
                expectASTPunctuatorToken(":"),
                expectASTType
            ])
        ),
        expectASTBlockStatement
    ])(context);
    if (!returnContext)
        return undefined;

    let identifier = <ASTIdentifier>returnContext.elements[1];
    let parameters: ASTParameterDeclaration[] = [];
    let returnType: Maybe<ASTType> = undefined;
    let body: Maybe<ASTStatement> = undefined;

    for (const elem of returnContext.elements) {
        if (elem instanceof ASTParameterDeclaration)
            parameters.push(elem);
        if (elem instanceof ASTType)
            returnType = elem;
        if (elem instanceof ASTStatement)
            body = elem;
    }
    if (!body)
        return undefined;
    let node = new ASTFunctionDeclarationStatement(
        identifier,
        parameters,
        body,
        returnType,
    );
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTCaseClause(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("case"),
        expectASTExpression,
        expectASTPunctuatorToken(":"),
        expectExpectorAnyCount(
            expectASTStatement
        )
    ])(context);
    if (!returnContext)
        return undefined;
    let expression = <ASTExpression>returnContext.elements[1];
    let statements: ASTStatement[] = [];
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTStatement)
            statements.push(elem);
    }
    let node = new ASTCaseClause(expression, statements);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTDefaultClause(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("default"),
        expectASTPunctuatorToken(":"),
        expectExpectorAnyCount(
            expectASTStatement
        )
    ])(context);
    if (!returnContext)
        return undefined;
    let statements: ASTStatement[] = [];
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTStatement)
            statements.push(elem);
    }
    let node = new ASTDefaultClause(statements);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTSwitchStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("switch"),
        expectASTPunctuatorToken("("),
        expectASTExpression,
        expectASTPunctuatorToken(")"),
        expectASTPunctuatorToken("{"),
        expectExpectorAnyCount(
            expectExpectorsOneOf([
                expectASTCaseClause,
                expectASTDefaultClause,
            ])
        ),
        expectASTPunctuatorToken("}"),
    ])(context);
    if (!returnContext)
        return undefined;
    let condition = <ASTExpression>returnContext.elements[2];
    let clauses: ASTStatement[] = [];
    for (const elem of returnContext.elements) {
        if (elem instanceof ASTStatement)
            clauses.push(elem);
    }
    let node = new ASTSwitchStatement(condition, clauses);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTImportClause(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectExpectorsOneOf([
            expectASTIdentifier,
            expectASTPunctuatorToken("*"),
        ]),
        expectMaybeExpector(
            expectExpectors([
                expectASTKeywordToken("as"),
                expectASTIdentifier
            ])
        )
    ])(context);
    if (!returnContext)
        return undefined;
    let item = returnContext.elements[0];
    let asName: Maybe<ASTIdentifier> = undefined;
    if (returnContext.elements.length != 1)
        asName = <ASTIdentifier>returnContext.elements[2];
    let node = new ASTImportClause(item, asName);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTImportStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectors([
        expectASTKeywordToken("import"),
        expectExpectorsOneOf([
            expectASTImportClause,
            expectExpectors([
                expectASTPunctuatorToken("{"),
                expectMaybeExpector(
                    expectExpectors([
                        expectASTImportClause,
                        expectExpectorAnyCount(
                            expectExpectors([
                                expectASTPunctuatorToken(","),
                                expectASTImportClause
                            ])
                        )
                    ])
                ),
                expectASTPunctuatorToken("}"),
            ])
        ]),
        expectASTKeywordToken("from"),
        expectASTStringLiteral
    ])(context);
    if (!returnContext)
        return undefined;
    let clauses: ASTImportClause[] = [];
    let moduleName: string = "";
    for (const node of returnContext.elements) {
        if (node instanceof ASTImportClause) {
            clauses.push(node);
        }
        if (node instanceof ASTStringLiteral) {
            moduleName = node.value.slice(1, node.value.length - 1);
        }
    }
    let node = new ASTImportStatement(clauses, moduleName);
    returnContext.elements = [node];
    return returnContext;
}

export function expectASTStatement(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    return expectExpectorsOneOf([
        expectASTVariableDeclarationStatement,
        expectASTExpressionStatement,
        expectASTIfStatement,
        expectASTSwitchStatement,
        expectASTWhileStatment,
        expectASTForStatement,
        expectASTDoWhileStatement,
        expectASTFunctionDeclaration,
        expectASTBlockStatement,
        expectASTReturnStatement,
        expectASTContinueStatement,
        expectASTBreakStatement,
        expectASTEmptyStatement,
        expectASTImportStatement,
    ])(context);
}

export function expectASTProgram(
    context: ASTExpectorContext
): Maybe<ASTExpectorReturnContext> {
    let returnContext = expectExpectorAnyCount(
        expectASTStatement
    )(context);
    if (!returnContext)
        return undefined;
    let node = new ASTProgram(returnContext.elements);
    returnContext.elements = [node];
    return returnContext;
}