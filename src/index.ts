import { Parser } from "./Parsers/index";
import { Tokenizer } from "./Tokenizer/index";
import { ProgramSourceLoader } from "./Program/index";
import { writeFileSync, fstat, createWriteStream, createReadStream, copyFileSync, existsSync } from "fs";
import {
    ASTNodeType, 
    ASTExpressionType, ASTLiteralType, ASTStatementType, ASTNode 
} from "./AST/index";
import { HalsteadMetrics, HalsteadMetricsAnalyzer } from "./HalsteadMetrics/HalsteadMetrics";
import { JilbMetrics, JilbMetricsAnalyzer } from "./JilbMetrics/JilbMetrics";
import { DataFlowComplexityMetricsAnalyzer } from "./DataFlowComplexityMetrics/DataFlowComplexityMetrics";

function backupFile(file_path: string, backup_file_path: string) {
    if (existsSync(file_path))
        copyFileSync(file_path, backup_file_path);
}

function getInFilePath(fileClass: string): string {
    return `./test/test_${fileClass}.in`;
}
function getMetricsFilePath(fileClass: string): string {
    return `./test/test_${fileClass}_metrics.out`;
}
function getMetricsBackupFilePath(fileClass: string): string {
    return `./test/test_${fileClass}_metrics_old.out`;
}

backupFile(getMetricsFilePath('Halstead'), getMetricsBackupFilePath('Halstead'));
backupFile(getMetricsFilePath('Jilb'), getMetricsBackupFilePath('Jilb'));
backupFile(getMetricsFilePath('DataFlowComplexity'), getMetricsBackupFilePath('DataFlowComplexity'));

function getASTNode(metricClass: string): ASTNode | undefined {
    let loader = new ProgramSourceLoader(getInFilePath(metricClass));
    loader.load();
    
    let tokenizer = new Tokenizer(loader.source);
    let tokens = tokenizer.tokenize();
    let parser = new Parser(tokens);
    let ast = parser.parse();
    let replacer = (key: string, value: any) => {
        if (["parent", "childs"].includes(key)) {
            return undefined;
        }
        if (key === "_type") {
            return ASTNodeType[value];
        }
        if (key === "expressionType") {
            return ASTExpressionType[value];
        }
        if (key === "literalType") {
            return ASTLiteralType[value];
        }
        if (key === "statementType") {
            return ASTStatementType[value];
        }
        return value;
    }
    if (!ast) {
        console.log("AST was not constructed");
    } else {
        writeFileSync(`./test/test_${metricClass}_tokens.out`, JSON.stringify(tokens, null, '\t'));
        writeFileSync(`./test/test_${metricClass}_ast.out`, JSON.stringify(ast, replacer, '\t'));
    }
    return ast;
}

let halsteadASTNode = getASTNode('Halstead');
if (halsteadASTNode) {
    let halsteadMetrics = new HalsteadMetricsAnalyzer().calcMetrics(halsteadASTNode);
    console.log('Halstead Ok');
    writeFileSync(getMetricsFilePath('Halstead'), halsteadMetrics.toString());
}

let jilbASTNode = getASTNode('Jilb');
if (jilbASTNode) {
    let jilbMetrics = new JilbMetricsAnalyzer().calcMetrics(jilbASTNode);
    console.log('Jilb Ok');
    writeFileSync(getMetricsFilePath('Jilb'), jilbMetrics.toString());
}

let dataFlowComplexityASTNode = getASTNode('DataFlowComplexity');
if (dataFlowComplexityASTNode) {
    let dataFlowComplexityMetrics = new DataFlowComplexityMetricsAnalyzer().calcMetrics(dataFlowComplexityASTNode);
    console.log('DataFlowComplexity Ok');
    writeFileSync(getMetricsFilePath('DataFlowComplexity'), dataFlowComplexityMetrics.toString());
}
