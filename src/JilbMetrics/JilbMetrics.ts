import { ASTNode, ASTProgram, ASTStatement, ASTVariableDeclarationStatement, ASTExpressionStatement, ASTBlockStatement, ASTIfStatement, ASTWhileStatement, ASTForStatement, ASTFunctionDeclarationStatement, ASTSwitchStatement, ASTCaseClause, ASTDefaultClause, ASTReturnStatement } from "../AST";

export class JilbMetrics {
    public conditional_operators_count: number = 0;
    public operators_count: number = 0;
    public conditional_operator_max_depth: number = 0;

    constructor() {
    }

    addConditionalOperator(depth: number = 0, name: string = "") {
        this.conditional_operators_count++;
        this.operators_count++;
        // console.log(`add operator \"${name}\"`);
        this.conditional_operator_max_depth = Math.max(this.conditional_operator_max_depth, depth);
    }

    addOperator(depth: number = 0, name: string = "") {
        this.operators_count++;
        // console.log(`add operator \"${name}\"`);
    }

    toString(): string {
        const PADDING = 4;
        const PADDING_STRING = ' '.repeat(PADDING);
        let resultString: string = "";

        resultString += `Абсолютная сложность программы:\n`;
        resultString += PADDING_STRING + `CL = ${this.conditional_operators_count}\n`;
        resultString += "\n";

        resultString += `Количество операторов:\n`;
        resultString += PADDING_STRING + `op_cnt = ${this.operators_count}\n`;
        resultString += "\n";

        resultString += `Относительная сложность программы:\n`;
        resultString += PADDING_STRING + `cl = ${this.conditional_operators_count * 1.0 / this.operators_count}\n`;
        resultString += "\n";

        resultString += `Максимальный уровень вложенности условного оператора:\n`;
        resultString += PADDING_STRING + `CLI = ${this.conditional_operator_max_depth}\n`;
        resultString += "\n";

        return resultString;
    }

}

export class JilbMetricsAnalyzer {
    private metrics: JilbMetrics = new JilbMetrics();
    constructor() {
    }

    calcMetrics(node: ASTNode): JilbMetrics {
        this.metrics = new JilbMetrics();
        this.calcASTNode(node);
        return this.metrics;
    }

    private calcASTNode(node: ASTNode) {
        if (node instanceof ASTProgram)
            return this.calcASTProgram(node);
    }

    private calcASTProgram(node: ASTProgram) {
        for (const statement of node.statements)
            this.calcASTStatement(<ASTStatement>statement);
    }

    private calcASTStatement(node: ASTStatement, depth: number = 0): void {
        if (node instanceof ASTVariableDeclarationStatement)
            return this.metrics.addOperator(depth, "variable declaration");
        if (node instanceof ASTExpressionStatement)
            return this.metrics.addOperator(depth, "expression statement");
        if (node instanceof ASTReturnStatement)
            return this.metrics.addOperator(depth, "return statement");
        if (node instanceof ASTBlockStatement) {
            for (const statement of node.statements)
                this.calcASTStatement(statement, depth);
            return;
        }
        if (node instanceof ASTIfStatement) {
            this.metrics.addConditionalOperator(depth, "if statement");

            for (const statement of node.ifStatements)
                this.calcASTStatement(statement, depth + 1);
            for (const statement of node.elseStatements)
                this.calcASTStatement(statement, depth + 1);
            return;
        }
        if (node instanceof ASTWhileStatement) {
            this.metrics.addConditionalOperator(depth, "while statement");
            return this.calcASTStatement(node.body, depth + 1);
        }
        if (node instanceof ASTForStatement) {
            if (node.initializer)
                this.calcASTStatement(node.initializer, depth);

            this.metrics.addConditionalOperator(depth, "for statement");
            this.calcASTStatement(node.body, depth + 1);

            if (node.afterExpression)
                this.metrics.addOperator(depth + 1, "for after statement");
            return;
        }
        if (node instanceof ASTFunctionDeclarationStatement)
            return this.calcASTStatement(node.body);
        if (node instanceof ASTSwitchStatement) {
            let currentDepth: number = depth;
            for (const clause of node.clauses) {
                if (clause instanceof ASTCaseClause) {
                    this.metrics.addConditionalOperator(currentDepth, "case statement");
                    currentDepth++;
                    for (const statement of clause.statements)
                        this.calcASTStatement(statement, currentDepth);
                }
                if (clause instanceof ASTDefaultClause) {
                    for (const statement of clause.statements)
                        this.calcASTStatement(statement, currentDepth);
                }
            }
            return;
        }
    }
}